#version 330
uniform mat4 projection;

in vec2 position;

void main() {

    gl_Position = vec4(-1.0, 1.0, 0.0, 0.0) +
            (projection * vec4(position.x, position.y, 0.0, 1.0));
}
