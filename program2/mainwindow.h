#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QActionGroup>
#include <QMenuBar>
#include "glwidget.h"
#include <glm/glm.hpp>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void contextMenuEvent(QContextMenuEvent *event);

private slots:
    void reset();
    void squares();
    void wall();
    void increaseBrickSizeX();
    void decreaseBrickSizeX();
    void increaseBrickSizeY();
    void decreaseBrickSizeY();
    void increaseBrickSizeZ();
    void decreaseBrickSizeZ();

    void increaseMortarSizeX();
    void decreaseMortarSizeX();
    void increaseMortarSizeY();
    void decreaseMortarSizeY();
    void increaseMortarSizeZ();
    void decreaseMortarSizeZ();

    void increaseWallSize();
    void decreaseWallSize();

    void toggleGrid();
    void toggleLight();
    void toggleModel();

private:
    GLWidget *myWidget;

    QMenu *fileMenu;
    QMenu *optionMenu;
    QMenu *shapeMenu;

    QActionGroup *myGroup;

    QAction *exitAction;
    QAction *resetAction;

    QAction *toggleGridAction;
    QAction *toggleLightAction;
    QAction *toggleModelAction;

    QAction *squaresAction;
    QAction *wallAction;

    QAction *brickLabel;
    QAction *brickXAction;
    QAction *brickYAction;
    QAction *brickZAction;
    QAction *brickXSmallAction;
    QAction *brickYSmallAction;
    QAction *brickZSmallAction;

    QAction *mortarLabel;
    QAction *mortarXAction;
    QAction *mortarYAction;
    QAction *mortarZAction;
    QAction *mortarXSmallAction;
    QAction *mortarYSmallAction;
    QAction *mortarZSmallAction;

    QAction *heightLabel;
    QAction *heightAction;
    QAction *heightSmallAction;

    glm::vec3 brickAdjust;
    glm::vec3 percentBrickAdjust;
};


#endif // MAINWINDOW_H
