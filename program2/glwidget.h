#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>

#define GLM_FORCE_RADIANS

using glm::mat4;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core { 
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);

        void setShape(int s);
        void adjustHeight(float grow);
        void resizeBricks(glm::vec3 shift);
        void changePercentBricks(glm::vec3 shift);

        void toggleGrid();
        void toggleLightTrackball();

    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);

    private:
        void initializeWall();
        void initializeSquareWallVao(std::vector<unsigned int> &indices,
                                         std::vector<glm::vec3> &positions,
                                         std::vector<glm::vec3> &normals);
        void initializeWallVao(std::vector<unsigned int> &indices,
                               std::vector<glm::vec3> &positions,
                               std::vector<glm::vec3> &normals);
        void renderWall();

        int shape;
        int modelSize;
        int squareModelSize;

        bool gridToggle;
        bool lightToggle;

        glm::vec3 brickSize;
        glm::vec3 percentBrick;
        glm::vec3 scalar;
/*
        bool strafeLeft;
        bool strafeRight;
        bool turnLeft;
        bool turnRight;
        bool moveFront;
        bool moveBack;
*/
        GLuint wallProg;
        GLuint wallVao;
        GLuint squareVao;

        GLint modelProjMatrixLoc;
        GLint modelViewMatrixLoc;
        GLint modelModelMatrixLoc;

        GLint modelLightPosLoc;
        GLint modelLightColorLoc;
        GLint modelLightIntensityLoc;

        GLint modelDiffuseColorLoc;
        GLint modelAmbientColorLoc;

        GLint brickColorLoc;
        GLint brickSizeLoc;
        GLint percentBrickLoc;
        GLint scalarLoc;

        void initializeGrid();
        void renderGrid();

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;

        int width;
        int height;

        glm::vec3 lastVPt;
        glm::vec3 lastLightPt;
        glm::vec3 pointOnVirtualTrackball(const glm::vec2 &pt);
};

#endif
