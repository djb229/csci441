HEADERS += glwidget.h \ 
    mainwindow.h
SOURCES += glwidget.cpp main.cpp \
    mainwindow.cpp

QT += opengl designer
CONFIG -= app_bundle
CONFIG += console c++11
INCLUDEPATH += "../include"
INCLUDEPATH += $$PWD

RESOURCES += shaders.qrc
