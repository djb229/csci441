#include "mainwindow.h"
#include "glwidget.h"

#include <QtGui>
#include <QVBoxLayout>
#include <stdio.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    myWidget = new GLWidget();
    setCentralWidget(myWidget);

    exitAction = new QAction(tr("E&xit"), this);
    exitAction->setShortcuts(QKeySequence::Quit);
    exitAction->setStatusTip(tr("Close"));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));

    squaresAction = new QAction(tr("Square"), this);
    squaresAction->setStatusTip(tr("Four walls"));
    connect(squaresAction, SIGNAL(triggered()), this, SLOT(squares()));

    wallAction = new QAction(tr("Wall"), this);
    wallAction->setStatusTip(tr("One wall"));
    connect(wallAction, SIGNAL(triggered()), this, SLOT(wall()));

    resetAction = new QAction(tr("Reset"), this);
    resetAction->setStatusTip(tr("Reset the wall"));
    connect(resetAction, SIGNAL(triggered()), this, SLOT(reset()));

    toggleGridAction = new QAction(tr("Toggle grid"), this);
    toggleGridAction->setStatusTip(tr("Toggle X-Z grid"));
    connect(toggleGridAction, SIGNAL(triggered()), this, SLOT(toggleGrid()));

    toggleLightAction = new QAction(tr("Toggle light movement"), this);
    toggleLightAction->setStatusTip(tr("Toggle movement of the light source"));
    toggleLightAction->setCheckable(true);
    toggleLightAction->setChecked(false);
    connect(toggleLightAction, SIGNAL(triggered()), this, SLOT(toggleLight()));

    toggleModelAction = new QAction(tr("Toggle model movement"), this);
    toggleModelAction->setStatusTip(tr("Toggle movement of the model"));
    toggleModelAction->setCheckable(true);
    toggleModelAction->setChecked(true);
    connect(toggleModelAction, SIGNAL(triggered()), this, SLOT(toggleModel()));

    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(resetAction);
    fileMenu->addAction(exitAction);

    optionMenu = menuBar()->addMenu(tr("&Options"));
    optionMenu->addAction(toggleGridAction);
    optionMenu->addSeparator();
    optionMenu->addAction(toggleLightAction);
    optionMenu->addAction(toggleModelAction);
    optionMenu->addSeparator();

    shapeMenu = menuBar()->addMenu(tr("&Shapes"));
    shapeMenu->addAction(squaresAction);
    shapeMenu->addAction(wallAction);

    brickLabel = new QAction(tr("Brick Size:"), this);
    brickLabel->setDisabled(true);
    brickXAction = new QAction(tr("+X"), this);
    brickXAction->setStatusTip(tr("Increase X size of the bricks"));
    connect(brickXAction, SIGNAL(triggered()), this, SLOT(increaseBrickSizeX()));
    brickYAction = new QAction(tr("+Y"), this);
    brickYAction->setStatusTip(tr("Increase Y size of the bricks"));
    connect(brickYAction, SIGNAL(triggered()), this, SLOT(increaseBrickSizeY()));
    brickZAction = new QAction(tr("+Z"), this);
    brickZAction->setStatusTip(tr("Increase Z size of the bricks"));
    connect(brickZAction, SIGNAL(triggered()), this, SLOT(increaseBrickSizeZ()));
    brickXSmallAction = new QAction(tr("-X"), this);
    brickXSmallAction->setStatusTip(tr("Decrease X size of the bricks"));
    connect(brickXSmallAction, SIGNAL(triggered()), this, SLOT(decreaseBrickSizeX()));
    brickYSmallAction = new QAction(tr("-Y"), this);
    brickYSmallAction->setStatusTip(tr("Decrease Y size of the bricks"));
    connect(brickYSmallAction, SIGNAL(triggered()), this, SLOT(decreaseBrickSizeY()));
    brickZSmallAction = new QAction(tr("-Z"), this);
    brickZSmallAction->setStatusTip(tr("Decrease Z size of the bricks"));
    connect(brickZSmallAction, SIGNAL(triggered()), this, SLOT(decreaseBrickSizeZ()));

    menuBar()->addAction(brickLabel);
    menuBar()->addAction(brickXAction);
    menuBar()->addAction(brickXSmallAction);
    menuBar()->addAction(brickYAction);
    menuBar()->addAction(brickYSmallAction);
    menuBar()->addAction(brickZAction);
    menuBar()->addAction(brickZSmallAction);

    mortarLabel = new QAction(tr("Mortar:Brick ratio:"), this);
    mortarLabel->setDisabled(true);
    mortarXAction = new QAction(tr("+X"), this);
    mortarXAction->setStatusTip(tr("Increase mortar:brick ratio about X"));
    connect(mortarXAction, SIGNAL(triggered()), this, SLOT(increaseMortarSizeX()));
    mortarYAction = new QAction(tr("+Y"), this);
    mortarYAction->setStatusTip(tr("Increase mortar:brick ratio about Y"));
    connect(mortarYAction, SIGNAL(triggered()), this, SLOT(increaseMortarSizeY()));
    mortarZAction = new QAction(tr("+Z"), this);
    mortarZAction->setStatusTip(tr("Increase mortar:brick ratio about Z"));
    connect(mortarZAction, SIGNAL(triggered()), this, SLOT(increaseMortarSizeZ()));
    mortarXSmallAction = new QAction(tr("-X"), this);
    mortarXSmallAction->setStatusTip(tr("Decrease mortar:brick ratio about X"));
    connect(mortarXSmallAction, SIGNAL(triggered()), this, SLOT(decreaseMortarSizeX()));
    mortarYSmallAction = new QAction(tr("-Y"), this);
    mortarYSmallAction->setStatusTip(tr("Decrease mortar:brick ratio about Y"));
    connect(mortarYSmallAction, SIGNAL(triggered()), this, SLOT(decreaseMortarSizeY()));
    mortarZSmallAction = new QAction(tr("-Z"), this);
    mortarZSmallAction->setStatusTip(tr("Decrease mortar:brick ratio about Z"));
    connect(mortarZSmallAction, SIGNAL(triggered()), this, SLOT(decreaseMortarSizeZ()));

    menuBar()->addAction(mortarLabel);
    menuBar()->addAction(mortarXAction);
    menuBar()->addAction(mortarXSmallAction);
    menuBar()->addAction(mortarYAction);
    menuBar()->addAction(mortarYSmallAction);
    menuBar()->addAction(mortarZAction);
    menuBar()->addAction(mortarZSmallAction);

    heightLabel = new QAction(tr("Height:"), this);
    heightLabel->setDisabled(true);
    heightAction = new QAction(tr("+Y"), this);
    heightAction->setStatusTip(tr("Increase height"));
    connect(heightAction, SIGNAL(triggered()), this, SLOT(increaseWallSize()));
    heightSmallAction = new QAction(tr("-Y"), this);
    heightSmallAction->setStatusTip(tr("Decrease height"));
    connect(heightSmallAction, SIGNAL(triggered()), this, SLOT(decreaseWallSize()));

    menuBar()->addAction(heightLabel);
    menuBar()->addAction(heightAction);
    menuBar()->addAction(heightSmallAction);

    brickAdjust = glm::vec3(0.05,0.05,0.05);
    percentBrickAdjust = glm::vec3(0.05,0.05,0.05);

    setWindowTitle(tr("Brick Walls"));
    setMinimumSize(160, 160);
    resize(1280, 960);
}

MainWindow::~MainWindow()
{

}

void MainWindow::contextMenuEvent(QContextMenuEvent *event) {
    QMenu menu(this);
    menu.exec(event->globalPos());
}
void MainWindow::squares() {
    myWidget->setShape(1);
}
void MainWindow::wall() {
    myWidget->setShape(0);
}

// Brick size adjustment methods.
void MainWindow::increaseBrickSizeX() {
    myWidget->resizeBricks(glm::vec3(0.05,0,0));
}
void MainWindow::decreaseBrickSizeX() {
    myWidget->resizeBricks(glm::vec3(-0.05,0,0));
}
void MainWindow::increaseBrickSizeY() {
    myWidget->resizeBricks(glm::vec3(0,0.05,0));
}
void MainWindow::decreaseBrickSizeY() {
    myWidget->resizeBricks(glm::vec3(0,-0.05,0));
}
void MainWindow::increaseBrickSizeZ() {
    myWidget->resizeBricks(glm::vec3(0,0,0.05));
}
void MainWindow::decreaseBrickSizeZ() {
    myWidget->resizeBricks(glm::vec3(0,0,-0.05));
}

// Methods to adjust spacing between bricks
void MainWindow::increaseMortarSizeX() {
    myWidget->changePercentBricks(glm::vec3(-0.05,0,0));
}
void MainWindow::decreaseMortarSizeX() {
    myWidget->changePercentBricks(glm::vec3(0.05,0,0));
}
void MainWindow::increaseMortarSizeY() {
    myWidget->changePercentBricks(glm::vec3(0,-0.05,0));
}
void MainWindow::decreaseMortarSizeY() {
    myWidget->changePercentBricks(glm::vec3(0,0.05,0));
}
void MainWindow::increaseMortarSizeZ() {
    myWidget->changePercentBricks(glm::vec3(0,0,-0.05));
}
void MainWindow::decreaseMortarSizeZ() {
    myWidget->changePercentBricks(glm::vec3(0,0,0.05));
}

// Methods to adjust wall size
void MainWindow::increaseWallSize() {
    myWidget->adjustHeight(1.0);
}
void MainWindow::decreaseWallSize() {
    myWidget->adjustHeight(-1.0);
}

// Reset GLWidget
void MainWindow::reset() {
    myWidget = new GLWidget;
    setCentralWidget(myWidget);
}

// Toggle menu options
void MainWindow::toggleGrid() {
    myWidget->toggleGrid();
}
void MainWindow::toggleLight() {
    bool modelChecked = toggleModelAction->isChecked();
    bool lightChecked = toggleLightAction->isChecked();
    if(lightChecked && modelChecked) {
        myWidget->toggleLightTrackball();
        toggleModelAction->setChecked(false);
    } else {
        toggleLightAction->setChecked(true);
    }
}
void MainWindow::toggleModel() {
    bool modelChecked = toggleModelAction->isChecked();
    bool lightChecked = toggleLightAction->isChecked();
    if(modelChecked && lightChecked) {
        myWidget->toggleLightTrackball();
        toggleLightAction->setChecked(false);
    } else {
        toggleModelAction->setChecked(true);
    }
}
