#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
    shape = 0;
}

GLWidget::~GLWidget() {
}


void GLWidget::initializeGrid() {
    gridToggle = true;
    glGenVertexArrays(1, &gridVao);
    glBindVertexArray(gridVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    vec3 pts[84];
    for(int i = -10; i <= 10; i++) {

        pts[2*(i+10)] = vec3(i, -.5f, 10);
        pts[2*(i+10)+1] = vec3(i, -.5f, -10);

        pts[2*(i+10)+42] = vec3(10,-.5f, i);
        pts[2*(i+10)+43] = vec3(-10,-.5f, i);
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(program);
    gridProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    gridProjMatrixLoc = glGetUniformLocation(program, "projection");
    gridViewMatrixLoc = glGetUniformLocation(program, "view");
    gridModelMatrixLoc = glGetUniformLocation(program, "model");
}

void GLWidget::initializeWallVao(vector<unsigned int> &indices,
                                 vector<vec3> &positions,
                                 vector<vec3> &normals){
        // top
    positions.push_back(vec3(4,2,0.5));    // 0
    positions.push_back(vec3(4,2,-0.5));   // 1
    positions.push_back(vec3(-4,2,-0.5));  // 2
    positions.push_back(vec3(-4,2,0.5));   // 3

        // bottom
    positions.push_back(vec3(4,-2,0.5));   // 4
    positions.push_back(vec3(-4,-2,0.5)); // 5
    positions.push_back(vec3(-4,-2,-0.5)); // 6
    positions.push_back(vec3(4,-2,-0.5));  // 7

        // front
    positions.push_back(vec3(4,2,0.5));    // 8
    positions.push_back(vec3(-4,2,0.5));   // 9
    positions.push_back(vec3(-4,-2,0.5));  // 10
    positions.push_back(vec3(4,-2,0.5));   // 11

        // back
    positions.push_back(vec3(-4,-2,-0.5)); // 12
    positions.push_back(vec3(-4,2,-0.5));  // 13
    positions.push_back(vec3(4,2,-0.5));   // 14
    positions.push_back(vec3(4,-2,-0.5));  // 15

        // right
    positions.push_back(vec3(4,-2,0.5));   // 16
    positions.push_back(vec3(4,-2,-0.5));  // 17
    positions.push_back(vec3(4,2,-0.5));   // 18
    positions.push_back(vec3(4,2,0.5));     // 19

        // left
    positions.push_back(vec3(-4,-2,0.5));  // 20
    positions.push_back(vec3(-4,2,0.5));   // 21
    positions.push_back(vec3(-4,2,-0.5));  // 22
    positions.push_back(vec3(-4,-2,-0.5)); // 23

    GLuint restart = 0xFFFFFFFF;
    indices.push_back(0);
    for(unsigned int i = 1; i < 24; i++) {
        if(i%4==0)
            indices.push_back(restart);

        indices.push_back(i);
    }

    for(int i = 0; i < 6; i++) {
        vec3 n;
        vec3 a = positions[4*i+0] - positions[4*i+1];
        vec3 b = positions[4*i+0] - positions[4*i+2];
        n = cross(a,b);

        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
    }
}

void GLWidget::initializeSquareWallVao(vector<unsigned int> &indices,
                                 vector<vec3> &positions,
                                 vector<vec3> &normals){
    vec3 tempWall[] = {
        // top
        vec3(4,2,0.5),    // 0
        vec3(4,2,-0.5),   // 1
        vec3(-4,2,-0.5),  // 2
        vec3(-4,2,0.5),   // 3

        // bottom
        vec3(4,-2,0.5),   // 4
        vec3(-4,-2,0.5), // 5
        vec3(-4,-2,-0.5), // 6
        vec3(4,-2,-0.5),  // 7

        // front
        vec3(4,2,0.5),    // 8
        vec3(-4,2,0.5),   // 9
        vec3(-4,-2,0.5),  // 10
        vec3(4,-2,0.5),   // 11

        // back
        vec3(-4,-2,-0.5), // 12
        vec3(-4,2,-0.5),  // 13
        vec3(4,2,-0.5),   // 14
        vec3(4,-2,-0.5),  // 15

        // right
        vec3(4,-2,0.5),   // 16
        vec3(4,-2,-0.5),  // 17
        vec3(4,2,-0.5),   // 18
        vec3(4,2,0.5),     // 19

        // left
        vec3(-4,-2,0.5),  // 20
        vec3(-4,2,0.5),   // 21
        vec3(-4,2,-0.5),  // 22
        vec3(-4,-2,-0.5) // 23
    };
    vec3 add;
    add = vec3(0,0,4);
    for(int j = 0; j < 24; j++) {
        vec3 temp = tempWall[j];
        temp += add;
        positions.push_back(temp);
    }
    for(int j = 0; j < 24; j++) {
        vec3 temp = tempWall[j];
        temp -= add;
        positions.push_back(temp);
    }
    for(int j = 0; j < 24; j++) {
        vec3 temp = vec3(tempWall[j].z + 4,tempWall[j].y,tempWall[j].x);
        positions.push_back(temp);
    }
    for(int j = 0; j < 24; j++) {
        vec3 temp = vec3(tempWall[j].z - 4,tempWall[j].y,tempWall[j].x);
        positions.push_back(temp);
    }

    GLuint restart = 0xFFFFFFFF;
    indices.push_back(0);
    for(unsigned int i = 1; i < 96; i++) {
        if(i%4==0)
            indices.push_back(restart);

        indices.push_back(i);
    }

    for(int i = 0; i < 24; i++) {
        vec3 n;
        vec3 a = positions[4*i+0] - positions[4*i+1];
        vec3 b = positions[4*i+0] - positions[4*i+2];
        n = cross(a,b);

        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
    }
}

void GLWidget::initializeWall() {
    // Initialize position data
    vector<vec3> positions, normals, positionsSquare, normalsSquare;
    vector<GLuint> indices, indicesSquare;
    initializeWallVao(indices,positions,normals);
    initializeSquareWallVao(indicesSquare,positionsSquare,normalsSquare);

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, positions.size()*sizeof(vec3), &positions[0], GL_STATIC_DRAW);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    GLuint normalBuffer;
    glGenBuffers(1, &normalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(vec3), &normals[0], GL_STATIC_DRAW);

    GLuint squarePositionBuffer;
    glGenBuffers(1, &squarePositionBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, squarePositionBuffer);
    glBufferData(GL_ARRAY_BUFFER, positionsSquare.size()*sizeof(vec3), &positionsSquare[0], GL_STATIC_DRAW);

    GLuint squareIndexBuffer;
    glGenBuffers(1, &squareIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, squareIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSquare.size()*sizeof(unsigned int), &indicesSquare[0], GL_STATIC_DRAW);

    GLuint squareNormalBuffer;
    glGenBuffers(1, &squareNormalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, squareNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, normalsSquare.size()*sizeof(vec3), &normalsSquare[0], GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    wallProg = program;

    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &wallVao);
    glGenVertexArrays(1, &squareVao);

    // Bind the attributes "position" and "normal" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    GLint normalIndex = glGetAttribLocation(program, "normal");
    GLint positionIndex = glGetAttribLocation(program, "position");

    glBindVertexArray(wallVao);

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

    glBindVertexArray(squareVao);

    glBindBuffer(GL_ARRAY_BUFFER, squarePositionBuffer);
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, squareNormalBuffer);
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, squareIndexBuffer);

    modelProjMatrixLoc = glGetUniformLocation(program, "projection");
    modelViewMatrixLoc = glGetUniformLocation(program, "view");
    modelModelMatrixLoc = glGetUniformLocation(program, "model");

    modelDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(program, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(program, "lightPos");
    modelLightColorLoc = glGetUniformLocation(program, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    brickColorLoc = glGetUniformLocation(program, "brickColor");
    brickSizeLoc = glGetUniformLocation(program, "brickSize");
    percentBrickLoc = glGetUniformLocation(program, "percentBrick");
    scalarLoc = glGetUniformLocation(program,"scalar");
    modelSize = indices.size();
    squareModelSize = indicesSquare.size();

    scalar = vec3(1.0,1.0,1.0);
    brickSize = vec3(0.3,0.15,0.2);
    percentBrick = vec3(0.9,0.9,0.9);
    lastLightPt = vec3(1,-1,1);
    lightToggle = false;

    glUniform3f(brickSizeLoc,brickSize.x,brickSize.y,brickSize.z);
    glUniform3f(percentBrickLoc,percentBrick.x,percentBrick.y,percentBrick.z);
    glUniform3f(scalarLoc,scalar.x,scalar.y,scalar.z);
    glUniform3f(brickColorLoc,0.9,0,0);

    glUniform3f(modelLightPosLoc, lastLightPt.x*10,lastLightPt.y*10,lastLightPt.z*10);
    glUniform3f(modelLightColorLoc, 1,1,1);
    glUniform1f(modelLightIntensityLoc, 1);

    glUniform3f(modelAmbientColorLoc, 0.1, 0.1, 0.1);
    glUniform3f(modelDiffuseColorLoc, 1.2, 1.2, 1.2);
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);
    initializeWall();
    initializeGrid();
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, 1.0f, 100.0f);
    viewMatrix = lookAt(vec3(0,0,-10),vec3(0,0,0),vec3(0,1,0));
    modelMatrix = mat4(1.0f);

    glUseProgram(wallProg);
    glUniformMatrix4fv(modelProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(modelModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderGrid();
    renderWall();
}

void GLWidget::renderWall() {
    glUseProgram(wallProg);
    int size = modelSize;
    switch(shape) {
        case 1:
            glBindVertexArray(squareVao);
            size = squareModelSize;
            break;
        default:
            glBindVertexArray(wallVao);
            break;
    }
    glDrawElements(GL_TRIANGLE_FAN, size, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    if(gridToggle) {
        glDrawArrays(GL_LINES, 0, 84);
    }
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);

    return program;
}

void GLWidget::setShape(int s) {
    shape = s;
    update();
}

void GLWidget::adjustHeight(float grow) {
    scalar += vec3(0,0.045*grow,0);

    glUseProgram(wallProg);
    glUniform3f(scalarLoc,scalar.x,scalar.y,scalar.z);
    update();
}

void GLWidget::resizeBricks(vec3 shift){
    brickSize += shift;

    glUseProgram(wallProg);
    glUniform3f(brickSizeLoc,brickSize.x,brickSize.y,brickSize.z);
    update();
}

void GLWidget::changePercentBricks(vec3 shift){
    percentBrick += shift;

    glUseProgram(wallProg);
    glUniform3f(percentBrickLoc,percentBrick.x,percentBrick.y,percentBrick.z);
    update();
}

void GLWidget::toggleGrid() {
    gridToggle = !gridToggle;
}

void GLWidget::toggleLightTrackball() {
    lightToggle = !lightToggle;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    if (lightToggle) {
        lastLightPt = normalize(pointOnVirtualTrackball(pt));
    } else {
        lastVPt = normalize(pointOnVirtualTrackball(pt));
    }
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    if(!lightToggle) {
        vec2 pt(event->x(), event->y());
        vec3 vPt = normalize(pointOnVirtualTrackball(pt));

        vec3 axis = cross(lastVPt, vPt);
//        vec3 axis = cross(vPt, lastVPt);
        if(length(axis) >= .00001) {
            axis = normalize(axis);
            float angle = acos(dot(vPt,lastVPt));
            mat4 r = rotate(mat4(1.0f), angle, axis);

            modelMatrix = r*modelMatrix;
//            viewMatrix = r*viewMatrix;

            glUseProgram(wallProg);
            glUniformMatrix4fv(modelModelMatrixLoc, 1, false, value_ptr(modelMatrix));
//            glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(viewMatrix));

            glUseProgram(gridProg);
            glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));
//            glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
        }
        lastVPt = vPt;
    } else {
        vec2 pt(event->x(), event->y());
        vec3 vPt = normalize(pointOnVirtualTrackball(pt));

        vec3 axis = cross(lastLightPt, vPt);\

        if(length(axis) >= .00001) {
            axis = normalize(axis);
            float angle = acos(dot(vPt,lastLightPt));
            mat4 r = rotate(mat4(1.0f), angle, axis);

            glm::vec4 rotatedVec = r*glm::vec4(lastLightPt,0);
            lastLightPt = vec3(rotatedVec.x,rotatedVec.y,rotatedVec.z);

            glUseProgram(wallProg);
            glUniform3f(modelLightPosLoc, lastLightPt.x*10,lastLightPt.y*10,lastLightPt.z*10);

            glUseProgram(gridProg);
            glUniform3f(modelLightPosLoc, lastLightPt.x,lastLightPt.y,lastLightPt.z);
        }
        lastLightPt = vPt;
    }
    update();
}

vec3 GLWidget::pointOnVirtualTrackball(const vec2 &pt) {
    float r = .5f;
    float rr = r*r;
    vec3 p;
    p.x = -1 + pt.x*(2.0f/width);
    p.y = -(float)height/width*(1-pt.y*(2.0f/height));

    float xx = p.x*p.x;
    float yy = p.y*p.y;

    if(xx+yy <= rr*.5) {
        p.z = sqrt(rr-(xx+yy));
    } else {
        p.z = rr*.5/sqrt(xx+yy);
    }

//    std::cout << p.x << ", " << p.y << ", " << p.z << std::endl;

    return p;
}
