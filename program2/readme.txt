David Bell
Computer Graphics
Program 2

This implementation of a brick wall model program meets all of the minimum requirements and goes a little beyond. Everything should be intuitive, as the GLWidget is embedded in a QT window with a menu bar. The File menu contains an "Exit" option that exits the program, and a "Reset" option which reinitializes the GLWidget. The Options menu contains three options. The first simply enables the user to turn the X-Z grid on and off. The next two toggle whether the virtual trackball controls the orientation of the grid and the wall, or if the trackball controls the position of the light. Like the previous labs, the trackball is controlled by clicking on the GLWidget. Shapes are under the Shapes menu. Brick size, brick spacing, and height adjustment buttons are all immediately accessable on the menu bar.

Only two shapes were implemented: a standard wall and four walls enclosing a square. Lab 6 served as the basis for this program, and I did not want to compute too many shapes or any complex shapes. My main focus for this lab was the implementation of the brick pattern and the lighting.

For some reason, I really wanted to implement everything related to the wall without using external textures. As such, the brick pattern is created entirely in the fragment shader using a few simple operations. The color of the brick is only affected by its position relative to the light.

A large portion of the creative aspect of this project went into making the bricks appear more three-dimentional. Like the brick pattern, this is accomplished entirely on the fragment shader by performing the calculations for diffuse lighting an additional time for the mortar color. This gives the effect of mortar in close proximity to brick generally being darker than mortar further from brick, while also being affected by the position of the light. This took a fair amount of experimentation, as the tutorials online all pointed to using texture and bump mapping.

A second effort for the creativity in the project went into making the position of the light position variable. To do this, the same calculations for the virtual trackball (and some additional scaling) were applied to the position of the light.

I did not meet all of my goals, and I hope to add a few things after turning this in: 

1) Add a grass texture to the grid. I was scared off by the the apparent time investment required by some of the tutorials online.

2) Add a spherical background to contain the wall. After implementing grass, I figured such a background would not be too difficult. I see it using a similar texture mapping procedure as grass combined with the virtual trackball.

3) Enable the user to change the camera position. I messed around a little with this, but I could not implement a reliable solution prior to the due date.