#include "paintscene.h"
#include <QtGui>

PaintScene::PaintScene(QWidget *parent) : QWidget(parent)
{
    radius = 5;
    shape = 0;
}

PaintScene::~PaintScene()
{

}


void PaintScene::setColor(const QColor &myColor)
{
    color = myColor;
}

void PaintScene::setShapeSize(int myRadius)
{
    radius = myRadius;
}

void PaintScene::setShape(int myShape)
{
    shape = myShape;
}

void PaintScene::setImage(QImage &myImage)
{
    image = myImage;
    imageReset = image;
}

void PaintScene::reset()
{
    image = imageReset;
    update();
}

void PaintScene::mousePressEvent(QMouseEvent *event)
{
    pt = event->pos();
    mousePressed = true;

}

void PaintScene::mouseMoveEvent(QMouseEvent *event)
{
    if (mousePressed)
    {
        QPoint pt2 = event->pos();
        QRgb rgb = imageReset.pixel(pt2);
        QPainter painter(&image);
        QBrush brush(rgb);
        QPen pen(rgb);
        painter.setBrush(brush);
        painter.setPen(pen);

        switch (shape) {
            case 1:
                painter.drawEllipse(pt2,radius,radius);
                break;
            default:
                painter.drawRect(QRect(pt,pt2).normalized().adjusted(-radius,-radius,radius,radius));
                break;
        }
        update(QRect(pt,pt2).normalized().adjusted(-radius,-radius,radius,radius));
        pt = pt2;
    }
}

void PaintScene::mouseReleaseEvent(QMouseEvent *event)
{
    mousePressed = false;
    QRgb rgb = imageReset.pixel(pt);
    QPainter painter(&image);
    QBrush brush(rgb);
    QPen pen(rgb);
    painter.setBrush(brush);
    painter.setPen(pen);
    QPoint pt2 = pt;
    pt2.setX(pt2.x()+radius);
    pt2.setY(pt2.y()+radius);
    QRect rect = QRect(pt,pt2).normalized().adjusted(-radius,-radius,radius,radius);
    switch (shape) {
        case 1:
            painter.drawEllipse(pt,radius,radius);
            break;
        default:
            painter.drawRect(rect);
            break;
    }

    update(rect);
}

void PaintScene::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRect paintArea = event->rect();
    painter.drawImage(paintArea,image,paintArea);
}

void PaintScene::open(QString fileName)
{
    if(!imageReset.load(fileName))
    {
        return;
    }
    image = imageReset.scaled(this->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
    imageReset = imageReset.scaled(this->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
    resize(image);
    update();
}

void PaintScene::save(QString fileName)
{
    image.save(fileName);
}

void PaintScene::resize(QImage resized)
{
    resized = resized.scaled(this->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
    QPainter painter(this);
    painter.drawImage(0,0,resized);
}

void PaintScene::resizeEvent(QResizeEvent *event)
{
    if (image.size() != this->size())
    {
        resize(image);
        update();
    }
    QWidget::resizeEvent(event);
}

void PaintScene::fillScene()
{
    int scalar = (radius*2);
    QPainter painter(&image);
    for(int i = 0; i < image.size().height(); i+=scalar)
    {
        for(int j = 0; j < image.size().width(); j+=scalar)
        {
            QPoint next(j,i);
            QRgb rgb = imageReset.pixel(j,i);
            QBrush brush(rgb);
            QPen pen(rgb);
            painter.setBrush(brush);
            painter.setPen(pen);
            QPoint pt2 = next;
            pt2.setX(pt2.x()+radius);
            pt2.setY(pt2.y()+radius);
            QRect rect = QRect(next,pt2).normalized().adjusted(-radius,-radius,radius,radius);
            switch (shape) {
                case 1:
                    painter.drawEllipse(next,radius,radius);
                    break;
                default:
                    painter.drawRect(rect);
                    break;
            }
            update(rect);
        }
    }
}
