#-------------------------------------------------
#
# Project created by QtCreator 2015-02-24T12:10:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Bell_Program1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    paintscene.cpp

HEADERS  += mainwindow.h \
    paintscene.h
