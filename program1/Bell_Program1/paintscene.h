#ifndef PAINTSCENE_H
#define PAINTSCENE_H


#include <QImage>
#include <QWidget>
#include <QColor>
#include <QPoint>

class PaintScene : public QWidget
{
    Q_OBJECT
public:
    explicit PaintScene(QWidget *parent = 0);
    ~PaintScene();

    void setColor(const QColor &myColor);
    void setShapeSize(int myRadius);
    void setShape(int myShape);
    void setImage(QImage &myImage);
    void open(QString fileName);
    void save(QString fileName);
    void reset();
    void resize(QImage resized);
    void fillScene();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    int radius;
    int shape;

    bool mousePressed;

    QPoint pt;
    QColor color;
    QImage image;
    QImage imageReset;
};

#endif // PAINTSCENE_H
