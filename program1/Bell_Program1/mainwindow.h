#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QActionGroup>
#include <QMenuBar>
#include <QGraphicsScene>
#include <QGraphicsView>

class PaintScene;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void contextMenuEvent(QContextMenuEvent *event);

private slots:
    void open();
    void save();
    void reset();
    void squares();
    void circles();
    void small();
    void large();
    void fillImage();

private:
    PaintScene *myScene;
    QWidget *myWidget;

    int shape;
    int radius;

    QMenu *fileMenu;
    QMenu *optionsMenu;
    QMenu *sizeMenu;
    QMenu *shapeMenu;

    QActionGroup *myGroup;

    QAction *openAction;
    QAction *saveAction;
    QAction *exitAction;
    QAction *resetAction;
    QAction *squaresAction;
    QAction *circlesAction;
    QAction *fillAction;
    QAction *smallAction;
    QAction *largeAction;
};

#endif // MAINWINDOW_H
