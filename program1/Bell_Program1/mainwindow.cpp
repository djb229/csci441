#include "mainwindow.h"
#include "paintscene.h"

#include <QtGui>
#include <QVBoxLayout>
#include <stdio.h>
#include <QFileDialog>
#include <iostream>
#include <QColor>
#include <QPainter>

#ifdef WINDOWS
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
#endif

char myPath[FILENAME_MAX];

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    GetCurrentDir(myPath, sizeof(myPath));

    myScene = new PaintScene;
    setCentralWidget(myScene);

    openAction = new QAction(tr("&Open"), this);
    openAction->setShortcuts(QKeySequence::Open);
    openAction->setStatusTip(tr("Open a new image"));
    connect(openAction, SIGNAL(triggered()), this, SLOT(open()));

    saveAction = new QAction(tr("&Save"), this);
    saveAction->setShortcuts(QKeySequence::Save);
    saveAction->setStatusTip(tr("Save image"));
    connect(saveAction, SIGNAL(triggered()), this, SLOT(save()));

    resetAction = new QAction(tr("&Reset"), this);
    resetAction->setStatusTip(tr("Reset the image"));
    connect(resetAction, SIGNAL(triggered()), this, SLOT(reset()));

    exitAction = new QAction(tr("E&xit"), this);
    exitAction->setShortcuts(QKeySequence::Quit);
    exitAction->setStatusTip(tr("Close"));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));

    squaresAction = new QAction(tr("Squares"), this);
    squaresAction->setStatusTip(tr("Set shape to squares"));
    connect(squaresAction, SIGNAL(triggered()), this, SLOT(squares()));

    circlesAction = new QAction(tr("Circles"), this);
    circlesAction->setStatusTip(tr("Set shape to circles"));
    connect(circlesAction, SIGNAL(triggered()), this, SLOT(circles()));

    smallAction = new QAction(tr("Small"), this);
    smallAction->setStatusTip(tr("Small brush size"));
    connect(smallAction, SIGNAL(triggered()), this, SLOT(small()));

    fillAction = new QAction(tr("Fill"), this);
    fillAction->setStatusTip(tr("Fill the image with current shape"));
    connect(fillAction, SIGNAL(triggered()), this, SLOT(fillImage()));

    largeAction = new QAction(tr("Large"), this);
    largeAction->setStatusTip(tr("Large brush size"));
    connect(largeAction, SIGNAL(triggered()), this, SLOT(large()));

    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(openAction);
    fileMenu->addAction(saveAction);
    fileMenu->addAction(resetAction);
    fileMenu->addAction(exitAction);

    optionsMenu = menuBar()->addMenu(tr("&Options"));
    optionsMenu->addAction(fillAction);

    shapeMenu = optionsMenu->addMenu(tr("Sha&pes"));
    shapeMenu->addAction(circlesAction);
    shapeMenu->addAction(squaresAction);

    sizeMenu = optionsMenu->addMenu(tr("&Sizes"));
    sizeMenu->addAction(smallAction);
    sizeMenu->addAction(largeAction);

    setWindowTitle(tr("Painterly"));
    setMinimumSize(160, 160);
    resize(960, 720);
}

MainWindow::~MainWindow()
{

}

void MainWindow::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.exec(event->globalPos());
}

void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Save File"), myPath,
                                                 tr("Images (*.png *.jpg *.bmp)"));
    myScene->open(fileName);
}

void MainWindow::save()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), myPath,
                                                 tr("Images (*.png *.jpg *.bmp)"));
    myScene->save(fileName);
}

void MainWindow::reset()
{
    myScene->reset();
}
void MainWindow::squares()
{
    myScene->setShape(0);
}
void MainWindow::circles()
{
    myScene->setShape(1);
}
void MainWindow::small()
{
    myScene->setShapeSize(5);
}
void MainWindow::large()
{
    myScene->setShapeSize(10);
}
void MainWindow::fillImage()
{
    myScene->fillScene();
}
