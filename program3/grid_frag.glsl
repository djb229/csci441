#version 330
uniform sampler2D tex;

uniform vec3 lightPos;
uniform vec3 lightPos2;
uniform vec3 lightColor;
uniform float lightIntensity;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform vec3 ambientColor;
uniform vec3 diffuseColor;

in vec3 pos;
in vec3 norm;
in vec2 uvCoords;

out vec4 color_out;

void main() {
    vec3 L;
    vec3 L2;
    vec3 N = normalize(norm);

    L = normalize(lightPos-pos);
    L2 = normalize(lightPos2-pos);

    vec3 diffuse = diffuseColor*dot(N,L);

    vec4 color = texture(tex,uvCoords);
    if (color.x > 0.8 && color.z > 0.8) {
        discard;
    }

    color_out = vec4(vec3(color.x,color.y,color.z)*lightIntensity*lightColor*diffuse+ambientColor,1);
}
