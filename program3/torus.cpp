#include "torus.h"
#include <iostream>
#include <QTextStream>
#include <tinyobjloader/tiny_obj_loader.h>

using glm::vec3;
using glm::cross;

using namespace std;

Torus::Torus(GLWidget *myWidget, int count) : MyShapes(myWidget,":/vert_general.glsl", ":/frag_general.glsl")
{
    drawMethod = GL_TRIANGLES;
    switch(count) {
        case 0:
            shift = vec3(-0.92,20,1.1);
            scale = vec3(21,1,21);
            break;
        case 1:
            shift = vec3(-0.92,5,1.1);
            scale = vec3(1,1,1);
            break;
        case 2:
            shift = vec3(-0.92,3,7.1);
            scale = vec3(2,2,2);
            break;

    }

    initialize();
}

Torus::~Torus()
{

}

void Torus::initColors() {
    widget->glUniform3f(lightPosLoc, 0,0,1000);
    widget->glUniform3f(lightPos2Loc, 0,0,0);
    widget->glUniform3f(lightColorLoc, 0.3,.7,0.2);
    widget->glUniform1f(lightIntensityLoc, 12.8);
    widget->glUniform3f(ambientColorLoc, 0.135, 0.2225, 0.1575);
    widget->glUniform3f(diffuseColorLoc, .54, .89, .63);
    widget->glUniform3f(specularColorLoc, 0.316228,0.316228,0.316228);
}

void Torus::constructShape(vector<vec3> &positionArray,
                           vector<vec3> &normalArray,
                           vector<GLuint> &indexArray) {
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;

    QFile filename(":/models/Torus.obj");
    string err = tinyobj::LoadObj(shapes, materials, filename);

    if(!err.empty()) {
        cerr << err << endl;
        exit(1);
    }

    for(size_t v = 0; v < shapes[0].mesh.positions.size() / 3; v++) {
        float x = shapes[0].mesh.positions[3*v+0];
        float y = shapes[0].mesh.positions[3*v+1];
        float z = shapes[0].mesh.positions[3*v+2];

        positionArray.push_back(vec3(scale.x*(x+shift.x),scale.y*(y+shift.y),scale.z*(z+shift.z)));
        normalArray.push_back(vec3(0,0,0));
    }

    for(size_t tri = 0; tri < shapes[0].mesh.indices.size() / 3; tri++) {
        unsigned int ind0 = shapes[0].mesh.indices[3*tri+0];
        unsigned int ind1 = shapes[0].mesh.indices[3*tri+1];
        unsigned int ind2 = shapes[0].mesh.indices[3*tri+2];

        vec3 n;
        vec3 a = positionArray[ind0] - positionArray[ind1];
        vec3 b = positionArray[ind0] - positionArray[ind2];
        n = cross(a,b);

        normalArray[ind0] += n;
        normalArray[ind1] += n;
        normalArray[ind2] += n;

        indexArray.push_back(ind0);
        indexArray.push_back(ind1);
        indexArray.push_back(ind2);

        normalArray.push_back(n);
        normalArray.push_back(n);
        normalArray.push_back(n);
    }
}
