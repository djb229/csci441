#ifndef RABBITSHIP_H
#define RABBITSHIP_H

#include "myshapes.h"

class RabbitShip: public MyShapes {

public:
    RabbitShip(GLWidget *myWidget);
    ~RabbitShip();
    void constructShape(vector<glm::vec3> &positionArray,
                        vector<glm::vec3> &normalArray,
                        vector<GLuint> &indexArray);
    void initColors();
};

#endif // RABBITSHIP_H
