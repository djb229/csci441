#include "rabbitship.h"
#include <iostream>
#include <QTextStream>
#include <tinyobjloader/tiny_obj_loader.h>

using glm::vec3;
using glm::cross;

using namespace std;

RabbitShip::RabbitShip(GLWidget *myWidget) : MyShapes(myWidget,":/vert_general.glsl", ":/frag_general.glsl")
{
    drawMethod = GL_TRIANGLES;

    initialize();
}

RabbitShip::~RabbitShip()
{

}

void RabbitShip::initColors() {
    widget->glUniform3f(lightPosLoc, 0,0,1000);
    widget->glUniform3f(lightPos2Loc, 0,0,0);
    widget->glUniform3f(lightColorLoc, 0.5,0.5,0.5);
    widget->glUniform1f(lightIntensityLoc, 10.0);
    widget->glUniform3f(ambientColorLoc, 0.05, 0.05, 0.05);
    widget->glUniform3f(diffuseColorLoc, 0.5, 0.5, 0.5);
    widget->glUniform3f(specularColorLoc, 0.7,0.7,0.7);
}

void RabbitShip::constructShape(vector<vec3> &positionArray,
                           vector<vec3> &normalArray,
                           vector<GLuint> &indexArray) {
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;

    QFile filename(":/models/bunny.obj");
    string err = tinyobj::LoadObj(shapes, materials, filename);

    if(!err.empty()) {
        cerr << err << endl;
        exit(1);
    }

    for(size_t v = 0; v < shapes[0].mesh.positions.size() / 3; v++) {
        float x = shapes[0].mesh.positions[3*v+0]+20;
        float y = shapes[0].mesh.positions[3*v+1];
        float z = shapes[0].mesh.positions[3*v+2]-50;

        positionArray.push_back(vec3(x,y,z));
        normalArray.push_back(vec3(0,0,0));
    }

    for(size_t tri = 0; tri < shapes[0].mesh.indices.size() / 3; tri++) {
        unsigned int ind0 = shapes[0].mesh.indices[3*tri+0];
        unsigned int ind1 = shapes[0].mesh.indices[3*tri+1];
        unsigned int ind2 = shapes[0].mesh.indices[3*tri+2];

        vec3 n;
        vec3 a = positionArray[ind0] - positionArray[ind1];
        vec3 b = positionArray[ind0] - positionArray[ind2];
        n = cross(a,b);

        normalArray[ind0] += n;
        normalArray[ind1] += n;
        normalArray[ind2] += n;

        indexArray.push_back(ind0);
        indexArray.push_back(ind1);
        indexArray.push_back(ind2);

        normalArray.push_back(n);
        normalArray.push_back(n);
        normalArray.push_back(n);
    }
}

