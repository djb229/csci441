HEADERS += glwidget.h \   
    torus.h \
    myshapes.h \
    cube.h \
    wall.h \
    rabbitship.h \
    asteroids.h \
    sun.h
SOURCES += glwidget.cpp main.cpp ../include/tinyobjloader/tiny_obj_loader.cc \
    torus.cpp \
    myshapes.cpp \
    cube.cpp \
    wall.cpp \
    rabbitship.cpp \
    asteroids.cpp \
    sun.cpp

QT += opengl designer
CONFIG -= app_bundle
CONFIG += console c++11
INCLUDEPATH += "../include"
INCLUDEPATH += $$PWD

RESOURCES += \
    shaders.qrc

DISTFILES +=
