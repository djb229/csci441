#version 330
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float lightIntensity;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform vec3 brickColor,brickSize,percentBrick;

uniform vec3 ambientColor;
uniform vec3 diffuseColor;

in vec3 pos;
in vec3 norm;
in vec3 MCposition;

out vec4 color_out;

void main() {
    vec3 L;
    vec3 N = normalize(norm);

    L = normalize(lightPos-pos);

    vec3 diffuse = diffuseColor*dot(N,L);

    vec3 position = MCposition / brickSize;
    if(fract(position.y*0.5) > 0.5){
        position.x += 0.5;
    }
    position = fract(position);

    vec3 brick = step(position, percentBrick);
    float useBrick = brick.x * brick.y * brick.z;
    vec3 mortarColor = normalize(lightPos-MCposition);
    if(useBrick <= 0) {
        mortarColor = mortarColor*dot(N,position);
        float grey = max(mortarColor.x,mortarColor.y);
        grey = max(grey,mortarColor.z);
        grey = max(grey, 0.3);
        grey = min(grey, 0.7);
        mortarColor = vec3(grey,grey,grey);
    }
    vec3 color = mix(mortarColor, brickColor, useBrick);
    position = normalize(lightPos-MCposition);
    color = color*dot(N,position);

    color_out = vec4(color*lightIntensity*lightColor*diffuse+ambientColor, 1);
}
