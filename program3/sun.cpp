#include "sun.h"

Sun::Sun(GLWidget *myWidget) : MyShapes(myWidget,":/vert_general.glsl", ":/frag_general.glsl") {
    drawMethod = GL_TRIANGLE_FAN;
    initialize();
}

Sun::~Sun()
{

}

void Sun::initColors() {
    widget->glUniform3f(lightPosLoc, 0,0,1000);
    widget->glUniform3f(lightPos2Loc, 0,0,0);
    widget->glUniform3f(lightColorLoc, 0.1,0.1,0.01);
    widget->glUniform1f(lightIntensityLoc, 51.0);
    widget->glUniform3f(ambientColorLoc, 0.24725, 0.1995, 0.0745);
    widget->glUniform3f(diffuseColorLoc, 0.75164, 0.60648, 0.22648);
    widget->glUniform3f(specularColorLoc, 0.628281,0.555802,0.366065);
}

void Sun::constructShape(vector<vec3> &positionArray,
                           vector<vec3> &normalArray,
                           vector<GLuint> &indexArray) {
    float root2 = sqrt(2);
    float a = 10*(root2-1.0);
    float b = sqrt(4-2*root2)*10/(4-2*root2);
    float ab45 = 5*root2;
    vec3 pts[18];
    pts[0] = vec3(0,0,-90);
    pts[1] = vec3(0,10,-90);
    pts[2] = vec3(a,b,-90);
    pts[3] = vec3(ab45,ab45,-90);
    pts[4] = vec3(b,a,-90);
    pts[5] = vec3(10,0,-90);
    pts[6] = vec3(b,-a,-90);
    pts[7] = vec3(ab45,-ab45,-90);
    pts[8] = vec3(a,-b,-90);
    pts[9] = vec3(0,-10,-90);
    pts[10] = vec3(-a,-b,-90);
    pts[11] = vec3(-ab45,-ab45,-90);
    pts[12] = vec3(-b,-a,-90);
    pts[13] = vec3(-10,0,-90);
    pts[14] = vec3(-b,a,-90);
    pts[15] = vec3(-ab45,ab45,-90);
    pts[16] = vec3(-a,b,-90);
    pts[17] = vec3(0,10,-90);

    for(GLuint i = 0; i < 18; i++) {
        positionArray.push_back(pts[i]);

        normalArray.push_back(vec3(0,0,1));

        indexArray.push_back(i);
    }
}
