#include "glwidget.h"
#include <functional>

#include <random>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>
#include <QTimer>
#include <iostream>
#include "myshapes.h"
#include "cube.h"
#include "torus.h"
#include "wall.h"
#include "rabbitship.h"
#include "asteroids.h"
#include "sun.h"


#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
    shape = 0;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(animate()));
    timer->start(16);
    viewPosition = vec3(0,0,0);
    velocity = vec3(0,0,0);
    forward = false;
    backward = false;
    left = false;
    right = false;
    turnLeft = false;
    turnRight = false;
    moveUp = false;
    moveDown = false;
    fly = false;
}

GLWidget::~GLWidget() {
}

/*
 * Procedure to load a BMP texture. Follows this tutorial:
 * http://www.opengl-tutorial.org/beginners-tutorials/tutorial-5-a-textured-cube/
 */
void GLWidget::loadBMP(const char * bmp, GLuint texture) {
    // Try loading the file
    FILE *path;
    path = fopen(bmp, "rb");
    if(path == NULL) {
        printf("dds file load error\n");
    }

    // Get metadata of BMP file from the file's header
    unsigned char header[54];
    if(fread(header,1,54,path) != 54){
        printf("error reading bmp header\n");
    }
    unsigned int dataPos = *(unsigned int*)&(header[0x0A]);
    unsigned int height = *(unsigned int*)&(header[0x16]);
    unsigned int width = *(unsigned int*)&(header[0x12]);
    unsigned int imageSize = *(unsigned int*)&(header[0x22]);

    if(imageSize == 0) {
        imageSize = width*height*3;
    }
    if(dataPos == 0) {
        dataPos = 54;
    }

    // Read the data into an array
    unsigned char textureData[imageSize];
    fread(textureData, 1, imageSize, path);

    // Bind the texture
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, textureData);
}


void GLWidget::initializeGrass() {

    int radius = 20;

    grassSize = 636;  // Total number of vertices generated 636
    grassIndexSize = 795; // Total number of indices, including restarts 795

    vec3 grass[grassSize];
    vec3 grassUVCoordinates[grassSize];
    GLuint grassIndices[grassIndexSize];

    GLuint restart = 0xFFFFFFFF;
    float root2 = 1.41421;
    float slope = 0.5*root2;
    float root3 = 1.7320508;

    int index = 0;
    GLuint grassIndex = 0;

    // Generate horizontal planes bounded by the circle 400 = y*y+x*x for grass texture
    for(int i = -radius; i < radius; i++) {

        // Generate vertices, uvCoordinates, and indices for horizontal planes
        float z = i*root2; // Z-coordinate
        float x = sqrt(float(radius*radius)-(z*z)); // X-coordinate
        float length = x+x; // Length of the plane

        grass[grassIndex] = vec3(x, -0.5, z);       // Vertex position
        grassUVCoordinates[grassIndex] = vec3(0, 0, 0);  // UV coordinates
        grassIndices[index++] = grassIndex++;       // Index

        grass[grassIndex] = vec3(x, 0.5, z);          // Vertex position
        grassUVCoordinates[grassIndex] = vec3(0, 1, 0);  // UV coordinates
        grassIndices[index++] = grassIndex++;       // Index

        grass[grassIndex] = vec3(-x, 0.5, z);             // Vertex position
        grassUVCoordinates[grassIndex] = vec3(length, 1, 0); // UV coordinates
        grassIndices[index++] = grassIndex++;           // Index

        grass[grassIndex] = vec3(-x, -0.5, z);          // Vertex position
        grassUVCoordinates[grassIndex] = vec3(length, 0, 0); // UV coordinates
        grassIndices[index++] = grassIndex++;           // Index

        grassIndices[index++] = restart; // Restart for glDrawElements
    }

    // Initialize Diagonal planes for grass texture
    for(int i = -29; i < 29; i++) {

        /* Maths to determine the intersections of the line
         * x = (-2*sqrt(2)*y)+b with the circle 400 = y*y+x*x
         *
         * Implements this equation:
         * http://mathworld.wolfram.com/Circle-LineIntersection.html
         */

        float r2 = float(radius*radius);

        // Create two points on the line y = x*sqrt(2)/2
        float Y1 = 15*root2;
        float X1 = i;
        float dy = -Y1;
        float dx = i+15;

        // Components for equation to give points of intersection between line and circle
        float D = (X1*dy)-(dx*Y1);  // Determinant of |V1,V2|
        dy -= Y1;                   // Y-distance
        dx -= X1;                   // X-distance
        float dr2 = (dx*dx)+(dy*dy);

        float X2 = ((D*dy)+(dx*sqrt(r2*dr2-(D*D))))/dr2;
        float Y2 = ((-D*dx)-(abs(dy)*sqrt(r2*dr2-(D*D))))/dr2;
        X1 = ((D*dy)-(dx*sqrt(r2*dr2-(D*D))))/dr2;
        Y1 = ((-D*dx)+(abs(dy)*sqrt(r2*dr2-(D*D))))/dr2;

        // Determine length of the current plane.
        dy = Y2-Y1;
        dx = X2-X1;
        float length = sqrt(dx*dx+dy*dy);

        grass[grassIndex] = vec3(X1, -0.5, Y1);     // Position
        grassUVCoordinates[grassIndex] = vec3(0, 0, 0);  // UV coordinates
        grassIndices[index++] = grassIndex++;       // Index

        grass[grassIndex] = vec3(X1, 0.5, Y1);     // Position
        grassUVCoordinates[grassIndex] = vec3(0, 1, 0);  // UV coordinates
        grassIndices[index++] = grassIndex++;       // Index

        grass[grassIndex] = vec3(X2, 0.5, Y2);         // Position
        grassUVCoordinates[grassIndex] = vec3(length, 1, 0); // UV coordinates
        grassIndices[index++] = grassIndex++;           // Index

        grass[grassIndex] = vec3(X2, -0.5, Y2);         // Position
        grassUVCoordinates[grassIndex] = vec3(length, 0, 0); // UV coordinates
        grassIndices[index++] = grassIndex++;           // Index

        grassIndices[index++] = restart; // Restart for glDrawElements

        grass[grassIndex] = vec3(-X1, -0.5, Y1);     // Position
        grassUVCoordinates[grassIndex] = vec3(0, 0, 0);  // UV coordinates
        grassIndices[index++] = grassIndex++;       // Index

        grass[grassIndex] = vec3(-X1, 0.5, Y1);     // Position
        grassUVCoordinates[grassIndex] = vec3(0, 1, 0);  // UV coordinates
        grassIndices[index++] = grassIndex++;       // Index

        grass[grassIndex] = vec3(-X2, 0.5, Y2);         // Position
        grassUVCoordinates[grassIndex] = vec3(length, 1, 0); // UV coordinates
        grassIndices[index++] = grassIndex++;           // Index

        grass[grassIndex] = vec3(-X2, -0.5, Y2);         // Position
        grassUVCoordinates[grassIndex] = vec3(length, 0, 0); // UV coordinates
        grassIndices[index++] = grassIndex++;           // Index

        grassIndices[index++] = restart; // Restart for glDrawElements
    }

    grassSize = grassIndex;
    grassIndexSize = index;

    vec3 normals[grassIndex];

    for(GLuint i = 0; i < grassIndex/4; i++) {

        int plane = 4*i;
        vec3 a = grass[plane] - grass[plane+1];
        vec3 b = grass[plane] - grass[plane+2];
        vec3 n = cross(a,b);
        n = vec3(0,1,0);
        normals[plane] = n;
        normals[plane+1] = n;
        normals[plane+2] = n;
        normals[plane+3] = n;
    }

    topSize = 18;  // Total number of vertices generated
    topIndexSize = 18; // Total number of indices, including restarts
    coneSize = 64;  // Total number of vertices generated
    coneIndexSize = 80; // Total number of indices, including restarts
    // Data for the top face of the cone

    // Data for the sides of the cone

    float a = 21.0*(root2-1.0);
    float b = sqrt(4-2*root2)*21.0/(4-2*root2);
    float ab45 = 10.5*root2;
    vec3 top[topSize];
    top[0] = vec3(0,-0.5,0);
    top[1] = vec3(0,-0.5,21.0);
    top[2] = vec3(a,-0.5,b);
    top[3] = vec3(ab45,-0.5,ab45);
    top[4] = vec3(b,-0.5,a);
    top[5] = vec3(21.0,-0.5,0);
    top[6] = vec3(b,-0.5,-a);
    top[7] = vec3(ab45,-0.5,-ab45);
    top[8] = vec3(a,-0.5,-b);
    top[9] = vec3(0,-0.5,-21.0);
    top[10] = vec3(-a,-0.5,-b);
    top[11] = vec3(-ab45,-0.5,-ab45);
    top[12] = vec3(-b,-0.5,-a);
    top[13] = vec3(-21.0,-0.5,0);
    top[14] = vec3(-b,-0.5,a);
    top[15] = vec3(-ab45,-0.5,ab45);
    top[16] = vec3(-a,-0.5,b);
    top[17] = vec3(0,-0.5,21.0);

    GLuint topIndices[topSize] = {
        0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
    };

    vec3 topNorms[topSize] = {
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0)
    };

    vec3 topUVCoords[topSize] = {
        vec3(20.4,20.4,0),
        vec3(top[1].x+21.0,top[1].z+21.0,0),
        vec3(top[2].x+21.0,top[2].z+21.0,0),
        vec3(top[3].x+21.0,top[3].z+21.0,0),
        vec3(top[4].x+21.0,top[4].z+21.0,0),
        vec3(top[5].x+21.0,top[5].z+21.0,0),
        vec3(top[6].x+21.0,top[6].z+21.0,0),
        vec3(top[7].x+21.0,top[7].z+21.0,0),
        vec3(top[8].x+21.0,top[8].z+21.0,0),
        vec3(top[9].x+21.0,top[9].z+21.0,0),
        vec3(top[10].x+21.0,top[10].z+21.0,0),
        vec3(top[11].x+21.0,top[11].z+21.0,0),
        vec3(top[12].x+21.0,top[12].z+21.0,0),
        vec3(top[13].x+21.0,top[13].z+21.0,0),
        vec3(top[14].x+21.0,top[14].z+21.0,0),
        vec3(top[15].x+21.0,top[15].z+21.0,0),
        vec3(top[16].x+21.0,top[16].z+21.0,0),
        vec3(top[17].x+21.0,top[17].z+21.0,0),
    };

    float aUnit = root2-1.0;
    float bUnit = sqrt(4-2*root2)/(4-2*root2);
    float ab45Unit = root2/2;

    vec3 cone[coneSize] = {
        vec3(0,-0.5,21.0),
        vec3(0,-21.0,1.0),
        vec3(a,-0.5,b),
        vec3(aUnit,-21.0,bUnit),
        //reset
        vec3(a,-0.5,b),
        vec3(aUnit,-21.0,bUnit),
        vec3(ab45,-0.5,ab45),
        vec3(ab45Unit,-21.0,ab45Unit),
        //reset
        vec3(ab45,-0.5,ab45),
        vec3(ab45Unit,-21.0,ab45Unit),
        vec3(b,-0.5,a),
        vec3(bUnit,-21.0,aUnit),
        //reset
        vec3(b,-0.5,a),
        vec3(bUnit,-21.0,aUnit),
        vec3(21.0,-0.5,0),
        vec3(1.0,-21.0,0),
        //reset
        vec3(21.0,-0.5,0),
        vec3(1.0,-21.0,0),
        vec3(b,-0.5,-a),
        vec3(bUnit,-21.0,-aUnit),
        //reset
        vec3(b,-0.5,-a),
        vec3(bUnit,-21.0,-aUnit),
        vec3(ab45,-0.5,-ab45),
        vec3(ab45Unit,-21.0,-ab45Unit),
        //reset
        vec3(ab45,-0.5,-ab45),
        vec3(ab45Unit,-21.0,-ab45Unit),
        vec3(a,-0.5,-b),
        vec3(aUnit,-21.0,-bUnit),
        //reset
        vec3(a,-0.5,-b),
        vec3(aUnit,-21.0,-bUnit),
        vec3(0,-0.5,-21.0),
        vec3(0,-21.0,-1.0),
        //reset
        vec3(0,-0.5,-21.0),
        vec3(0,-21.0,-1.0),
        vec3(-a,-0.5,-b),
        vec3(-aUnit,-21.0,-bUnit),
        //reset
        vec3(-a,-0.5,-b),
        vec3(-aUnit,-21.0,-bUnit),
        vec3(-ab45,-0.5,-ab45),
        vec3(-ab45Unit,-21.0,-ab45Unit),
        //reset
        vec3(-ab45,-0.5,-ab45),
        vec3(-ab45Unit,-21.0,-ab45Unit),
        vec3(-b,-0.5,-a),
        vec3(-bUnit,-21.0,-aUnit),
        //reset
        vec3(-b,-0.5,-a),
        vec3(-bUnit,-21.0,-aUnit),
        vec3(-21.0,-0.5,0),
        vec3(-1.0,-21.0,0),
        //reset
        vec3(-21.0,-0.5,0),
        vec3(-1.0,-21.0,0),
        vec3(-b,-0.5,a),
        vec3(-bUnit,-21.0,aUnit),
        //reset
        vec3(-b,-0.5,a),
        vec3(-bUnit,-21.0,aUnit),
        vec3(-ab45,-0.5,ab45),
        vec3(-ab45Unit,-21.0,ab45Unit),
        //reset
        vec3(-ab45,-0.5,ab45),
        vec3(-ab45Unit,-21.0,ab45Unit),
        vec3(-a,-0.5,b),
        vec3(-aUnit,-21.0,bUnit),
        //reset
        vec3(-a,-0.5,b),
        vec3(-aUnit,-21.0,bUnit),
        vec3(0,-0.5,21.0),
        vec3(0,-21.0,1.0)
    };
    GLuint coneIndices[coneIndexSize] = {
        0,1,2,3,restart,
        4,5,6,7,restart,
        8,9,10,11,restart,
        12,13,14,15,restart,
        16,17,18,19,restart,
        20,21,22,23,restart,
        24,25,26,27,restart,
        28,29,30,31,restart,
        32,33,34,35,restart,
        36,37,38,39,restart,
        40,41,42,43,restart,
        44,45,46,47,restart,
        48,49,50,51,restart,
        52,53,54,55,restart,
        56,57,58,59,restart,
        60,61,62,63,restart
    };
    vec3 coneNorms[coneSize];
    vec3 coneUVCoords[coneSize];
    for(int i = 0; i < 16; i++) {
        vec3 a = cone[i*4] - cone[i*4+1];
        vec3 b = cone[i*4] - cone[i*4+2];
        vec3 n = normalize(cross(a,b));
        coneNorms[i*4] = n;
        if(i%2==1) {
            vec3 n2 = normalize(coneNorms[(i-1)*4] + coneNorms[i*4]);
           coneNorms[(i-1)*4] = n2;
           coneNorms[(i-1)*4+1] = n2;
           coneNorms[(i-1)*4+2] = n2;
           coneNorms[(i-1)*4+3] = n2;

           coneNorms[i*4+1] = n2;
           coneNorms[i*4+2] = n2;
           coneNorms[i*4+3] = n2;
        }

        coneUVCoords[i*4] = vec3(cone[i*4].x+1.0,cone[i*4].z+1.0,0);
        coneUVCoords[i*4+1] = vec3(cone[i*4+1].x+21.0,cone[i*4+1].z+21.0,0);
        coneUVCoords[i*4+2] = vec3(cone[i*4+2].x+1.0,cone[i*4+2].z+1.0,0);
        coneUVCoords[i*4+3] = vec3(cone[i*4+3].x+21.0,cone[i*4+3].z+21.0,0);
    }
    coneNorms[60] = coneNorms[61];


    // Generate textures
//    glGenTextures(3, textures);
    glGenTextures(4, textures);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, textures[2]);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, textures[3]);


    /* Load our vertex and fragment shaders into a program object on the GPU */
    grassProg = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(grassProg);

    // Load grass position, normal, UV position, and index data onto the GPU
    glGenVertexArrays(1, &grassVao);// Create vertex array object to hold grass vertex data
    glBindVertexArray(grassVao);

    GLuint grassPosBuffer;   // Create a buffer on the GPU for grass vertex coordinates
    glGenBuffers(1, &grassPosBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, grassPosBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(grass), grass, GL_STATIC_DRAW);
    // Bind attribute "position" to grassPosBuffer
    GLint grassPosIndex = glGetAttribLocation(grassProg, "position");
    glEnableVertexAttribArray(grassPosIndex);
    glVertexAttribPointer(grassPosIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint grassNormBuffer;  // Create a buffer on the GPU for grass vertex normals
    glGenBuffers(1, &grassNormBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, grassNormBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);
    // Bind attribute "normal" to grassNormBuffer
    GLint grassNormIndex = glGetAttribLocation(grassProg, "normal");
    glEnableVertexAttribArray(grassNormIndex);
    glVertexAttribPointer(grassNormIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint grassIndexBuffer; // Create a buffer on the GPU for grass vertex indices
    glGenBuffers(1, &grassIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, grassIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(grassIndices), grassIndices, GL_STATIC_DRAW);

    GLuint grassUVBuffer;    // Create a buffer on the GPU for grass UV coordinates
    glGenBuffers(1, &grassUVBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, grassUVBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(grassUVCoordinates), grassUVCoordinates, GL_STATIC_DRAW);
    // Bind attribute "uvCoordinates" to grassUVBuffer
    GLint grassUVIndex = glGetAttribLocation(grassProg, "uvCoordinates");
    glEnableVertexAttribArray(grassUVIndex);
    glVertexAttribPointer(grassUVIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // Load grass texture
    GLint grassTexIndex = glGetAttribLocation(grassProg, "tex");
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(grassTexIndex, 0);
    loadBMP("C:/Users/David/Documents/Downloads/grasstext3.bmp", textures[0]);
    glBindTexture(GL_TEXTURE_2D,textures[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Get model, view, and projection matrix locations
    grassProjMatrixLoc = glGetUniformLocation(grassProg, "projection");
    grassViewMatrixLoc = glGetUniformLocation(grassProg, "view");
    grassModelMatrixLoc = glGetUniformLocation(grassProg, "model");

    // Get lighting uniform locations
    grassLightPosLoc = glGetUniformLocation(grassProg, "lightPos");
    grassLightPos2Loc = glGetUniformLocation(grassProg, "lightPos2");
    grassLightColorLoc = glGetUniformLocation(grassProg, "lightColor");
    grassLightIntensityLoc = glGetUniformLocation(grassProg, "lightIntensity");
    grassDiffuseColorLoc = glGetUniformLocation(grassProg, "diffuseColor");

    // Set lighting uniform values
    glUniform3f(grassLightPosLoc, 100,10000,-100);
    glUniform3f(grassLightPos2Loc, -100,10000,100);
    glUniform3f(grassLightColorLoc, .4,.7,.1);
    glUniform1f(grassLightIntensityLoc, 1);
    glUniform3f(grassAmbientColorLoc, 0.4, 0.4, 0.4);
    glUniform3f(grassDiffuseColorLoc, 1, 1, 1);




    // Load our vertex and fragment shaders into a program object on the GPU
    topProg = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(topProg);

    // Load top position, normal, UV position, and index data onto the GPU
    glGenVertexArrays(1, &topVao);  // Create vertex array object to hold top vertex data
    glBindVertexArray(topVao);

    GLuint topPosBuffer;   // Create a buffer on the GPU for top UV coordinates
    glGenBuffers(1, &topPosBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, topPosBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(top), top, GL_STATIC_DRAW);
    // Bind attribute "position" to topPosBuffer
    GLint topPosIndex = glGetAttribLocation(topProg, "position");
    glEnableVertexAttribArray(topPosIndex);
    glVertexAttribPointer(topPosIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint topNormBuffer;  // Create a buffer on the GPU for top vertex normals
    glGenBuffers(1, &topNormBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, topNormBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(topNorms), topNorms, GL_STATIC_DRAW);
    // Bind attribute "normal" to topNormBuffer
    GLint topNormIndex = glGetAttribLocation(topProg, "normal");
    glEnableVertexAttribArray(topNormIndex);
    glVertexAttribPointer(topNormIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint topIndexBuffer; // Create a buffer on the GPU for top vertex indices
    glGenBuffers(1, &topIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, topIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(topIndices), topIndices, GL_STATIC_DRAW);

    GLuint topUVBuffer;    // Create a buffer on the GPU for top vertex coordinates
    glGenBuffers(1, &topUVBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, topUVBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(topUVCoords), topUVCoords, GL_STATIC_DRAW);
    // Bind attribute "uvCoordinates" to topUVBuffer
    GLint topUVIndex = glGetAttribLocation(topProg, "uvCoordinates");
    glEnableVertexAttribArray(topUVIndex);
    glVertexAttribPointer(topUVIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // Load top texture
    GLint topTexIndex = glGetAttribLocation(topProg, "tex");
    glActiveTexture(GL_TEXTURE1);
    loadBMP("C:/Users/David/Documents/Downloads/conetext.bmp", textures[1]);
    //glBindTexture(GL_TEXTURE_2D,textures[1]);
    glUniform1i(topTexIndex, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Get model, view, and projection matrix locations
    topProjMatrixLoc = glGetUniformLocation(topProg, "projection");
    topViewMatrixLoc = glGetUniformLocation(topProg, "view");
    topModelMatrixLoc = glGetUniformLocation(topProg, "model");

    // Get lighting uniform locations
    topLightPosLoc = glGetUniformLocation(topProg, "lightPos");
    topLightPos2Loc = glGetUniformLocation(topProg, "lightPos2");
    topLightColorLoc = glGetUniformLocation(topProg, "lightColor");
    topLightIntensityLoc = glGetUniformLocation(topProg, "lightIntensity");
    topDiffuseColorLoc = glGetUniformLocation(topProg, "diffuseColor");

    // Set lighting uniform values
    glUniform3f(topLightPosLoc, 100,10000,-100);
    glUniform3f(topLightPos2Loc, -100,10000,100);
    glUniform3f(topLightColorLoc, .4,.8,.2);
    glUniform1f(topLightIntensityLoc, 1);
    glUniform3f(topAmbientColorLoc, 0.4, 0.4, 0.4);
    glUniform3f(topDiffuseColorLoc, 1, 1, 1);




    // Load our vertex and fragment shaders into a program object on the GPU
    coneProg = loadShaders(":/vert_general.glsl", ":/frag_general.glsl");
    glUseProgram(coneProg);

    // Load cone position, normal, UV position, and index data onto the GPU
    glGenVertexArrays(1, &coneVao); // Create vertex array object to hold cone vertex data
    glBindVertexArray(coneVao);

    GLuint conePosBuffer;   // Create a buffer on the GPU for cone vertex coordinates
    glGenBuffers(1, &conePosBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, conePosBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cone), cone, GL_STATIC_DRAW);
    // Bind attribute "position" to conePosBuffer
    GLint conePosIndex = glGetAttribLocation(coneProg, "position");
    glEnableVertexAttribArray(conePosIndex);
    glVertexAttribPointer(conePosIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint coneNormBuffer;  // Create a buffer on the GPU for cone vertex normals
    glGenBuffers(1, &coneNormBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, coneNormBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(coneNorms), coneNorms, GL_STATIC_DRAW);
    // Bind attribute "normal" to topNormBuffer
    GLint coneNormIndex = glGetAttribLocation(coneProg, "normal");
    glEnableVertexAttribArray(coneNormIndex);
    glVertexAttribPointer(coneNormIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint coneIndexBuffer; // Create a buffer on the GPU for cone vertex indices
    glGenBuffers(1, &coneIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, coneIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(coneIndices), coneIndices, GL_STATIC_DRAW);

    // Get model, view, and projection matrix locations
    coneProjMatrixLoc = glGetUniformLocation(coneProg, "projection");
    coneViewMatrixLoc = glGetUniformLocation(coneProg, "view");
    coneModelMatrixLoc = glGetUniformLocation(coneProg, "model");

    // Get lighting uniform locations
    coneLightPosLoc = glGetUniformLocation(coneProg, "lightPos");
    coneLightPos2Loc = glGetUniformLocation(coneProg, "lightPos2");
    coneLightColorLoc = glGetUniformLocation(coneProg, "lightColor");
    coneLightIntensityLoc = glGetUniformLocation(coneProg, "lightIntensity");
    coneDiffuseColorLoc = glGetUniformLocation(coneProg, "diffuseColor");
    GLint coneMatColorLoc = glGetUniformLocation(coneProg, "materialColor");
    GLint coneSpecColorLoc = glGetUniformLocation(coneProg, "specularColor");

    // Set lighting uniform values
//    glUniform3f(coneLightPosLoc, 100,100,-100);
//    glUniform3f(coneLightPos2Loc, 100,-100,100);
    glUniform3f(coneLightPosLoc,1,-20,100);
    glUniform3f(coneLightPos2Loc,1,-10,50);
    glUniform3f(coneLightColorLoc, 1,1,1);
    glUniform1f(coneLightIntensityLoc, 3.0);
    glUniform3f(coneAmbientColorLoc, 0.21, 0.13, 0.05);
    glUniform3f(coneDiffuseColorLoc, 0.4, 0.4, 0.4);
    glUniform3f(coneMatColorLoc,.8,.9,1.0);
    glUniform3f(coneSpecColorLoc,0.775,0.775,0.775);


    // Initialize light location
    lastLightPt = vec3(0,100,0);
}

void GLWidget::initializeWallVao(vector<unsigned int> &indices,
                                 vector<vec3> &positions,
                                 vector<vec3> &normals){
        // top
    positions.push_back(vec3(4,2,0.5));    // 0
    positions.push_back(vec3(4,2,-0.5));   // 1
    positions.push_back(vec3(-4,2,-0.5));  // 2
    positions.push_back(vec3(-4,2,0.5));   // 3

        // bottom
    positions.push_back(vec3(4,-2,0.5));   // 4
    positions.push_back(vec3(-4,-2,0.5)); // 5
    positions.push_back(vec3(-4,-2,-0.5)); // 6
    positions.push_back(vec3(4,-2,-0.5));  // 7

        // front
    positions.push_back(vec3(4,2,0.5));    // 8
    positions.push_back(vec3(-4,2,0.5));   // 9
    positions.push_back(vec3(-4,-2,0.5));  // 10
    positions.push_back(vec3(4,-2,0.5));   // 11

        // back
    positions.push_back(vec3(-4,-2,-0.5)); // 12
    positions.push_back(vec3(-4,2,-0.5));  // 13
    positions.push_back(vec3(4,2,-0.5));   // 14
    positions.push_back(vec3(4,-2,-0.5));  // 15

        // right
    positions.push_back(vec3(4,-2,0.5));   // 16
    positions.push_back(vec3(4,-2,-0.5));  // 17
    positions.push_back(vec3(4,2,-0.5));   // 18
    positions.push_back(vec3(4,2,0.5));     // 19

        // left
    positions.push_back(vec3(-4,-2,0.5));  // 20
    positions.push_back(vec3(-4,2,0.5));   // 21
    positions.push_back(vec3(-4,2,-0.5));  // 22
    positions.push_back(vec3(-4,-2,-0.5)); // 23

    GLuint restart = 0xFFFFFFFF;
    indices.push_back(0);
    for(unsigned int i = 1; i < 24; i++) {
        if(i%4==0)
            indices.push_back(restart);

        indices.push_back(i);
    }

    for(int i = 0; i < 6; i++) {
        vec3 n;
        vec3 a = positions[4*i+0] - positions[4*i+1];
        vec3 b = positions[4*i+0] - positions[4*i+2];
        n = cross(a,b);

        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
    }
}

void GLWidget::initializeSquareWallVao(vector<unsigned int> &indices,
                                 vector<vec3> &positions,
                                 vector<vec3> &normals){
    vec3 tempWall[] = {
        // top
        vec3(4,2,0.5),    // 0
        vec3(4,2,-0.5),   // 1
        vec3(-4,2,-0.5),  // 2
        vec3(-4,2,0.5),   // 3

        // bottom
        vec3(4,-2,0.5),   // 4
        vec3(-4,-2,0.5), // 5
        vec3(-4,-2,-0.5), // 6
        vec3(4,-2,-0.5),  // 7

        // front
        vec3(4,2,0.5),    // 8
        vec3(-4,2,0.5),   // 9
        vec3(-4,-2,0.5),  // 10
        vec3(4,-2,0.5),   // 11

        // back
        vec3(-4,-2,-0.5), // 12
        vec3(-4,2,-0.5),  // 13
        vec3(4,2,-0.5),   // 14
        vec3(4,-2,-0.5),  // 15

        // right
        vec3(4,-2,0.5),   // 16
        vec3(4,-2,-0.5),  // 17
        vec3(4,2,-0.5),   // 18
        vec3(4,2,0.5),     // 19

        // left
        vec3(-4,-2,0.5),  // 20
        vec3(-4,2,0.5),   // 21
        vec3(-4,2,-0.5),  // 22
        vec3(-4,-2,-0.5) // 23
    };
    vec3 add;
    add = vec3(0,0,4);
    for(int j = 0; j < 24; j++) {
        vec3 temp = tempWall[j];
        temp += add;
        positions.push_back(temp);
    }
    for(int j = 0; j < 24; j++) {
        vec3 temp = tempWall[j];
        temp -= add;
        positions.push_back(temp);
    }
    for(int j = 0; j < 24; j++) {
        vec3 temp = vec3(tempWall[j].z + 4,tempWall[j].y,tempWall[j].x);
        positions.push_back(temp);
    }
    for(int j = 0; j < 24; j++) {
        vec3 temp = vec3(tempWall[j].z - 4,tempWall[j].y,tempWall[j].x);
        positions.push_back(temp);
    }

    GLuint restart = 0xFFFFFFFF;
    indices.push_back(0);
    for(unsigned int i = 1; i < 96; i++) {
        if(i%4==0)
            indices.push_back(restart);

        indices.push_back(i);
    }

    for(int i = 0; i < 24; i++) {
        vec3 n;
        vec3 a = positions[4*i+0] - positions[4*i+1];
        vec3 b = positions[4*i+0] - positions[4*i+2];
        n = cross(a,b);

        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
    }
}

void GLWidget::initializeWall() {
    // Initialize position data
    vector<vec3> positions, normals, positionsSquare, normalsSquare;
    vector<GLuint> indices, indicesSquare;
    initializeWallVao(indices,positions,normals);
    initializeSquareWallVao(indicesSquare,positionsSquare,normalsSquare);

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, positions.size()*sizeof(vec3), &positions[0], GL_STATIC_DRAW);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    GLuint normalBuffer;
    glGenBuffers(1, &normalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(vec3), &normals[0], GL_STATIC_DRAW);

    GLuint squarePositionBuffer;
    glGenBuffers(1, &squarePositionBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, squarePositionBuffer);
    glBufferData(GL_ARRAY_BUFFER, positionsSquare.size()*sizeof(vec3), &positionsSquare[0], GL_STATIC_DRAW);

    GLuint squareIndexBuffer;
    glGenBuffers(1, &squareIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, squareIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSquare.size()*sizeof(unsigned int), &indicesSquare[0], GL_STATIC_DRAW);

    GLuint squareNormalBuffer;
    glGenBuffers(1, &squareNormalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, squareNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, normalsSquare.size()*sizeof(vec3), &normalsSquare[0], GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    wallProg = program;

    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &wallVao);
    glGenVertexArrays(1, &squareVao);

    // Bind the attributes "position" and "normal" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    GLint normalIndex = glGetAttribLocation(program, "normal");
    GLint positionIndex = glGetAttribLocation(program, "position");

    glBindVertexArray(wallVao);

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

    glBindVertexArray(squareVao);

    glBindBuffer(GL_ARRAY_BUFFER, squarePositionBuffer);
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, squareNormalBuffer);
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, squareIndexBuffer);

    modelProjMatrixLoc = glGetUniformLocation(program, "projection");
    modelViewMatrixLoc = glGetUniformLocation(program, "view");
    modelModelMatrixLoc = glGetUniformLocation(program, "model");

    modelDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(program, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(program, "lightPos");
    modelLightColorLoc = glGetUniformLocation(program, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    brickColorLoc = glGetUniformLocation(program, "brickColor");
    brickSizeLoc = glGetUniformLocation(program, "brickSize");
    percentBrickLoc = glGetUniformLocation(program, "percentBrick");
    scalarLoc = glGetUniformLocation(program,"scalar");
    modelSize = indices.size();
    squareModelSize = indicesSquare.size();

    scalar = vec3(1.0,1.0,1.0);
    brickSize = vec3(0.3,0.15,0.2);
    percentBrick = vec3(0.9,0.9,0.9);

    glUniform3f(brickSizeLoc,brickSize.x,brickSize.y,brickSize.z);
    glUniform3f(percentBrickLoc,percentBrick.x,percentBrick.y,percentBrick.z);
    glUniform3f(scalarLoc,scalar.x,scalar.y,scalar.z);
    glUniform3f(brickColorLoc,0.9,0,0);

    glUniform3f(modelLightPosLoc, 1,10000,100);
    glUniform3f(modelLightColorLoc, 1,1,1);
    glUniform1f(modelLightIntensityLoc, 1);

    glUniform3f(modelAmbientColorLoc, 0.1, 0.1, 0.1);
    glUniform3f(modelDiffuseColorLoc, 1.2, 1.2, 1.2);
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    lastLightPt = vec3(1,10,100);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(1.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);
    initializeWall();
    initializeGrass();
    initializeStars();

//    bool sides[] = {false,false,true,true,true,true};

    cube = new Cube(this,vec3(2,1,1),vec3(2,1,2));
    wall = new Wall(this,vec3(1,1,1),vec3(1,1,1));
    torus = new Torus(this,0);
    torus2 = new Torus(this,1);
    torus3 = new Torus(this,2);
    rabbit = new RabbitShip(this);
    asteroids = new Asteroids(this,vec3(1,1,1),vec3(0,0,0));
    sun = new Sun(this);

    viewMatrix = lookAt(vec3(0,0,-10),vec3(0,0,0),vec3(0,1,0));
    modelMatrix = mat4(1.0f);
    yawMatrix = mat4(1.0f);
    pitchMatrix = mat4(1.0f);
    translateMatrix = lookAt(vec3(0,-1.5,-10),vec3(0,-1.5,0),vec3(0,1,0));

    pitchAngle = 0;
    yawAngle = 0;
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, 1.0f, 100.0f);
    modelMatrix = mat4(1.0f);

    glUseProgram(wallProg);
    glUniformMatrix4fv(modelProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(modelModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(grassProg);
    glUniformMatrix4fv(grassProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(grassViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(grassModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(topProg);
    glUniformMatrix4fv(topProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(topViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(topModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(coneProg);
    glUniformMatrix4fv(coneProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(coneViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(coneModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(starsProg);
    glUniformMatrix4fv(starsProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(starsViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(starsModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    cube->updateMatrices(value_ptr(modelMatrix),value_ptr(viewMatrix),value_ptr(projMatrix));
    wall->updateMatrices(value_ptr(modelMatrix),value_ptr(viewMatrix),value_ptr(projMatrix));
    torus->updateMatrices(value_ptr(modelMatrix),value_ptr(viewMatrix),value_ptr(projMatrix));
    torus2->updateMatrices(value_ptr(modelMatrix),value_ptr(viewMatrix),value_ptr(projMatrix));
    torus3->updateMatrices(value_ptr(modelMatrix),value_ptr(viewMatrix),value_ptr(projMatrix));
    rabbit->updateMatrices(value_ptr(modelMatrix),value_ptr(viewMatrix),value_ptr(projMatrix));
    asteroids->updateMatrices(value_ptr(modelMatrix),value_ptr(viewMatrix),value_ptr(projMatrix));
    sun->updateMatrices(value_ptr(modelMatrix),value_ptr(viewMatrix),value_ptr(projMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    renderSky();
    renderGrass();
//    renderWall();
    cube->render();
    wall->render();
    torus->render();
    torus2->render();
    torus3->render();
    rabbit->render();
    asteroids->render();
    sun->render();
}

void GLWidget::renderSky() {
    glUseProgram(starsProg);
    glBindVertexArray(starsVao);
    glDrawArrays(GL_POINTS,0,600);
    //glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderWall() {
    glUseProgram(wallProg);
    int size = modelSize;
    switch(shape) {
        case 1:
            glBindVertexArray(squareVao);
            size = squareModelSize;
            break;
        default:
            glBindVertexArray(wallVao);
            break;
    }
    glDrawElements(GL_TRIANGLE_FAN, size, GL_UNSIGNED_INT, 0);
}


void GLWidget::renderGrass() {
    glUseProgram(grassProg);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,textures[0]);
    glBindVertexArray(grassVao);
    glDrawElements(GL_TRIANGLE_FAN, grassIndexSize, GL_UNSIGNED_INT, 0);

    glUseProgram(topProg);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,textures[1]);
    glBindVertexArray(topVao);
    glDrawElements(GL_TRIANGLE_FAN, topIndexSize, GL_UNSIGNED_INT, 0);

    glUseProgram(coneProg);
    glBindVertexArray(coneVao);
    glDrawElements(GL_TRIANGLE_STRIP, coneIndexSize, GL_UNSIGNED_INT, 0);

}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);

    return program;
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            // forward
            forward = true;
            break;
        case Qt::Key_A:
            // left
            left = true;
            break;
        case Qt::Key_D:
            // right
            right = true;
            break;
        case Qt::Key_S:
            backward = true;
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            fly = !fly;
            break;
        case Qt::Key_Shift:
            // down
            down = true;
            break;
        case Qt::Key_Space:
            // up or jump
            up = true;
            break;
        case Qt::Key_Down:
            moveDown = true;
            break;
        case Qt::Key_Up:
            // toggle fly mode
            moveUp = true;
            break;
        case Qt::Key_Left:
            // down
            turnLeft = true;
            break;
        case Qt::Key_Right:
            // up or jump
            turnRight = true;
            break;
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            // forward
            forward = false;
            //velocity = velocity - vectorForward;
            break;
        case Qt::Key_A:
            // left
            left = false;
            //velocity = velocity - vectorRight;
            break;
        case Qt::Key_D:
            // right
            right = false;
            //velocity = velocity - vectorRight;
            break;
        case Qt::Key_S:
            // backward
            backward = false;
            //velocity = velocity - vectorForward;
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            break;
        case Qt::Key_Shift:
            // down
            down = false;
            break;
        case Qt::Key_Space:
            // up or jump
            up = false;
            break;
        case Qt::Key_Down:
            moveDown = false;
            break;
        case Qt::Key_Up:
            moveUp = false;
            break;
        case Qt::Key_Left:
            turnLeft = false;
            break;
        case Qt::Key_Right:
            turnRight = false;
            break;
    }
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastPt = pt;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec2 d = pt-lastPt;
    d = normalize(d);

    float y = atan(-d.x);

    if(y > 0.1) {
        y = 0.1;
    } else if (y < -0.1) {
        y = -0.1;
    }

    yawAngle += y;

    yawMatrix = rotate(mat4(1.0),yawAngle,vec3(0,1.0,0));

    // Pitch
    float p = atan(-d.y);

    if(p > 0.09) {
        p = 0.09;
    } else if (p < -0.09) {
        p = -0.09;
    }

    pitchAngle += p;

    if(pitchAngle > M_PI/2) {
        pitchAngle = M_PI/2;
    } else if (pitchAngle < -M_PI/2) {
        pitchAngle = -M_PI/2;
    }

    pitchMatrix = rotate(mat4(1.0),pitchAngle,vec3(1.0,0,0));

    lastPt = pt;
}

vec3 GLWidget::pointOnVirtualTrackball(const vec2 &pt) {
    float r = .5f;
    float rr = r*r;
    vec3 p;
    p.x = -1 + pt.x*(2.0f/width);
    p.y = -(float)height/width*(1-pt.y*(2.0f/height));

    float xx = p.x*p.x;
    float yy = p.y*p.y;

    if(xx+yy <= rr*.5) {
        p.z = sqrt(rr-(xx+yy));
    } else {
        p.z = rr*.5/sqrt(xx+yy);
    }

    return p;
}

void GLWidget::animate() {
    vec3 rightVector,forwardVector,newViewPosition;

    if (turnLeft) {
        yawAngle += 0.08;
        yawMatrix = rotate(mat4(1.0),yawAngle,vec3(0,1.0,0));
    } else if (turnRight) {
        yawAngle -= 0.08;
        yawMatrix = rotate(mat4(1.0),yawAngle,vec3(0,1.0,0));
    }

    if (moveDown) {
        pitchAngle -= 0.05;

        if(pitchAngle > M_PI/2) {
            pitchAngle = M_PI/2;
        } else if (pitchAngle < -M_PI/2) {
            pitchAngle = -M_PI/2;
        }

        pitchMatrix = rotate(mat4(1.0),pitchAngle,vec3(1.0,0,0));
    } else if (moveUp) {
        pitchAngle += 0.05;

        if(pitchAngle > M_PI/2) {
            pitchAngle = M_PI/2;
        } else if (pitchAngle < -M_PI/2) {
            pitchAngle = -M_PI/2;
        }

        pitchMatrix = rotate(mat4(1.0),pitchAngle,vec3(1.0,0,0));
    }

    if (right||left) {
        rightVector = vec3(yawMatrix[0].x,yawMatrix[0].y,yawMatrix[0].z);
        rightVector = normalize(rightVector);
        if (left) {
            rightVector = -rightVector;
        }
        velocity = velocity + rightVector;
        velocity = normalize(velocity);
    }

    if (!fly && (backward||forward)) {
        forwardVector = vec3(yawMatrix[2].x,yawMatrix[2].y,yawMatrix[2].z);
        forwardVector = normalize(forwardVector);
        if (forward) {
            forwardVector = -forwardVector;
        }
        velocity = velocity + forwardVector;
        velocity = normalize(velocity);
    } else if (backward||forward) {
        mat4 yawPitch = yawMatrix*pitchMatrix;
        forwardVector = vec3(yawPitch[2].x,yawPitch[2].y,yawPitch[2].z);
        forwardVector = normalize(forwardVector);
        if (forward) {
            forwardVector = -forwardVector;
        }
        velocity = velocity + forwardVector;
        velocity = normalize(velocity);
    }

    velocity = velocity * 0.1f;
    newViewPosition = velocity;
    velocity = vec3(0,0,0);
    translateMatrix = glm::translate(translateMatrix,newViewPosition);
    viewMatrix = translateMatrix*yawMatrix*pitchMatrix;
    viewMatrix = inverse(viewMatrix);

    glUseProgram(wallProg);
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(viewMatrix));

    glUseProgram(grassProg);
    glUniformMatrix4fv(grassViewMatrixLoc, 1, false, value_ptr(viewMatrix));

    glUseProgram(topProg);
    glUniformMatrix4fv(topViewMatrixLoc, 1, false, value_ptr(viewMatrix));

    glUseProgram(coneProg);
    glUniformMatrix4fv(coneViewMatrixLoc, 1, false, value_ptr(viewMatrix));

    mat4 invYawPitch = inverse(yawMatrix*pitchMatrix);

    glUseProgram(starsProg);
    glUniformMatrix4fv(starsViewMatrixLoc, 1, false, value_ptr(invYawPitch));
    glUniform1i(starColorLoc, starColorArray[starColorIndex/5]);
    if (starColorIndex == 600) {
        starColorIndex = 0;
    } else {
        starColorIndex++;
    }

    cube->updateMatrices(false,value_ptr(viewMatrix),false);
    wall->updateMatrices(false,value_ptr(viewMatrix),false);
    torus->updateMatrices(false,value_ptr(viewMatrix),false);
    torus2->updateMatrices(false,value_ptr(viewMatrix),false);
    torus3->updateMatrices(false,value_ptr(viewMatrix),false);
    rabbit->updateMatrices(false,value_ptr(viewMatrix),false);
    asteroids->updateMatrices(false,value_ptr(viewMatrix),false);
    sun->updateMatrices(false,value_ptr(invYawPitch),false);

    update();
}

void GLWidget::initializeStars() {
    float radius = 100.0;

    std::default_random_engine engine;
    std::uniform_real_distribution<float> distribution(-radius,radius);
    auto random = std::bind(distribution,engine);

    vec3 starArray[600];
    float radius2 = radius*radius;

    for(int i = 0; i < 100; i++) {
        float a = random();
        float b = random();
        float c = sqrt(radius2-(a*a + b*b));
        starArray[i] = vec3(a,b,-c);
        starColorArray[i] = i;
    }
    for(int i = 100; i < 200; i++) {
        float a = random();
        float b = random();
        float c = sqrt(radius2-(a*a + b*b));
        starArray[i] = vec3(a,b,c);
        starColorArray[i] = i;
    }
    for(int i = 200; i < 300; i++) {
        float a = random();
        float b = random();
        float c = sqrt(radius2-(a*a + b*b));
        starArray[i] = vec3(a,-c,b);
        starColorArray[i] = i;
    }
    for(int i = 300; i < 400; i++) {
        float a = random();
        float b = random();
        float c = sqrt(radius2-(a*a + b*b));
        starArray[i] = vec3(a,c,b);
        starColorArray[i] = i;
    }
    for(int i = 400; i < 500; i++) {
        float a = random();
        float b = random();
        float c = sqrt(radius2-(a*a + b*b));
        starArray[i] = vec3(-c,a,b);
        starColorArray[i] = i;
    }
    for(int i = 500; i < 600; i++) {
        float a = random();
        float b = random();
        float c = sqrt(radius2-(a*a + b*b));
        //starArray[i] = vec3(70.0,random(),random());
        starArray[i] = vec3(c,a,b);
        starColorArray[i] = i;
    }

    starsProg = loadShaders(":/sky_vert.glsl", ":/sky_frag.glsl");
    glUseProgram(starsProg);
    glGenVertexArrays(1,&starsVao);
    glBindVertexArray(starsVao);

    GLuint posBuffer;   // Create a buffer on the GPU for star vertex coordinates
    glGenBuffers(1, &posBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, posBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(starArray), starArray, GL_STATIC_DRAW);
    // Bind attribute "position" to posBuffer
    GLint posIndex = glGetAttribLocation(starsProg, "position");
    glEnableVertexAttribArray(posIndex);
    glVertexAttribPointer(posIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    starColorIndex = 0;

    starColorLoc = glGetUniformLocation(starsProg, "colorChange");
    glUniform1i(starColorLoc, starColorArray[starColorIndex++]);

    starsProjMatrixLoc = glGetUniformLocation(starsProg, "projection");
    starsViewMatrixLoc = glGetUniformLocation(starsProg, "view");
    starsModelMatrixLoc = glGetUniformLocation(starsProg, "model");
}
