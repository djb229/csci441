#ifndef _TORUS_H
#define _TORUS_H

#include "myshapes.h"

class Torus: public MyShapes {

public:
    Torus(GLWidget *myWidget, int count);
    ~Torus();
    void constructShape(vector<glm::vec3> &positionArray,
                        vector<glm::vec3> &normalArray,
                        vector<GLuint> &indexArray);
    void initColors();

    vec3 shift;
    vec3 scale;
};

#endif // TORUS_H
