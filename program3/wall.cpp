#include "wall.h"

using glm::vec3;
using glm::cross;

Wall::Wall(GLWidget *myWidget, glm::vec3 scale, glm::vec3 shift) :
    MyShapes(myWidget, ":/vert.glsl", ":/frag.glsl") {



    drawMethod = GL_TRIANGLE_FAN;

    initialize();
}

Wall::~Wall()
{

}

void Wall::constructShape(vector<vec3> &positionArray,
                          vector<vec3> &normalArray,
                          vector<GLuint> &indexArray) {

        // top
    positionArray.push_back(vec3(4,2,0.5));    // 0
    positionArray.push_back(vec3(4,2,-0.5));   // 1
    positionArray.push_back(vec3(-4,2,-0.5));  // 2
    positionArray.push_back(vec3(-4,2,0.5));   // 3

        // bottom
    positionArray.push_back(vec3(4,-2,0.5));   // 4
    positionArray.push_back(vec3(-4,-2,0.5)); // 5
    positionArray.push_back(vec3(-4,-2,-0.5)); // 6
    positionArray.push_back(vec3(4,-2,-0.5));  // 7

        // front
    positionArray.push_back(vec3(4,2,0.5));    // 8
    positionArray.push_back(vec3(-4,2,0.5));   // 9
    positionArray.push_back(vec3(-4,-2,0.5));  // 10
    positionArray.push_back(vec3(4,-2,0.5));   // 11

        // back
    positionArray.push_back(vec3(-4,-2,-0.5)); // 12
    positionArray.push_back(vec3(-4,2,-0.5));  // 13
    positionArray.push_back(vec3(4,2,-0.5));   // 14
    positionArray.push_back(vec3(4,-2,-0.5));  // 15

        // right
    positionArray.push_back(vec3(4,-2,0.5));   // 16
    positionArray.push_back(vec3(4,-2,-0.5));  // 17
    positionArray.push_back(vec3(4,2,-0.5));   // 18
    positionArray.push_back(vec3(4,2,0.5));     // 19

        // left
    positionArray.push_back(vec3(-4,-2,0.5));  // 20
    positionArray.push_back(vec3(-4,2,0.5));   // 21
    positionArray.push_back(vec3(-4,2,-0.5));  // 22
    positionArray.push_back(vec3(-4,-2,-0.5)); // 23

    GLuint restart = 0xFFFFFFFF;
    indexArray.push_back(0);
    for(unsigned int i = 1; i < 24; i++) {
        if(i%4==0)
            indexArray.push_back(restart);

        indexArray.push_back(i);
    }

    for(int i = 0; i < 6; i++) {
        vec3 n;
        vec3 a = positionArray[4*i+0] - positionArray[4*i+1];
        vec3 b = positionArray[4*i+0] - positionArray[4*i+2];
        n = cross(a,b);

        normalArray.push_back(n);
        normalArray.push_back(n);
        normalArray.push_back(n);
        normalArray.push_back(n);
    }
}

void Wall::initColors() {

    GLint brickColorLoc = widget->glGetUniformLocation(program, "brickColor");
    GLint brickSizeLoc = widget->glGetUniformLocation(program, "brickSize");
    GLint percentBrickLoc = widget->glGetUniformLocation(program, "percentBrick");
    GLint scalarLoc = widget->glGetUniformLocation(program,"scalar");

    widget->glUniform3f(brickSizeLoc,0.3,0.15,0.2);
    widget->glUniform3f(percentBrickLoc,0.9,0.9,0.9);
    widget->glUniform3f(scalarLoc,1.0,1.0,1.0);
    widget->glUniform3f(brickColorLoc,0.9,0,0);

    widget->glUniform3f(lightPosLoc, 1,-20,1000);
    widget->glUniform3f(lightPos2Loc, 1,-10,5000);
    widget->glUniform3f(lightColorLoc, 1,1,1);
    widget->glUniform1f(lightIntensityLoc, 1);

    widget->glUniform3f(ambientColorLoc, 0.1, 0.1, 0.1);
    widget->glUniform3f(diffuseColorLoc, 1.2, 1.2, 1.2);
    widget->glUniform3f(specularColorLoc, 0.6,0.2,0.2);
}
