#version 330
uniform vec3 lightPos;
uniform vec3 lightPos2;
uniform vec3 lightColor;
uniform float lightIntensity;

uniform vec3 materialColor;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform vec3 ambientColor;
uniform vec3 diffuseColor;
uniform vec3 specularColor;

in vec3 pos;
in vec3 norm;

out vec4 color_out;

void main() {
    vec3 L = normalize(lightPos-pos);
    vec3 N = normalize(norm);
    vec3 R = normalize(-reflect(L,N));
    vec3 E = normalize(-pos);
    vec3 diffuse = diffuseColor*max(dot(N,L), 0);
    //diffuse = clamp(diffuse,0.0,1.0);
    vec3 specular = specularColor*pow(max(dot(R,E),0), 80.0);

    vec3 L2 = normalize(lightPos2-pos);


    color_out = vec4(lightIntensity*lightColor*diffuse+ambientColor+specular,1);
}
