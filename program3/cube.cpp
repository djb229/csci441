#include "cube.h"

using glm::vec3;
using glm::cross;

Cube::Cube(GLWidget *myWidget, glm::vec3 scale, glm::vec3 shift) :
    MyShapes(myWidget, ":/vert.glsl", ":/frag.glsl") {

    drawMethod = GL_TRIANGLE_FAN;
    this->scale = scale;
    translate = shift;

//    this->sides[0] = sides[0];
//    this->sides[1] = sides[1];
//    this->sides[2] = sides[2];
//    this->sides[3] = sides[3];
//    this->sides[4] = sides[4];
//    this->sides[5] = sides[5];
    sides[0]=true;
    sides[1]=true;
    sides[2]=true;
    sides[3]=true;
    sides[4]=true;
    sides[5]=true;

    initialize();
}

Cube::~Cube() {}

void Cube::constructShape(vector<vec3> &positionArray,
                          vector<vec3> &normalArray,
                          vector<GLuint> &indexArray) {

    float translateX = 4.0;
    float translateY = 1.0;
    float translateZ = 4.0;

    for(int j = 0; j < 4; j++) {
        translateX = translateX*pow(-1.0,float(j%2));
        translateZ = translateZ*pow(-1.0,float(j/2));

        float x = 1.0*scale.x+translate.x;
        float y = 1.0*scale.y+translate.y;
        float z = 1.0*scale.z+translate.z;

        size = 0;
        // top
        positionArray.push_back(vec3(x,y,z));    // 0
        positionArray.push_back(vec3(x,y,-z));   // 1
        positionArray.push_back(vec3(-x,y,-z));  // 2
        positionArray.push_back(vec3(-x,y,z));   // 3
        size+=4;

        // bottom
        positionArray.push_back(vec3(x,-y,z));   // 4
        positionArray.push_back(vec3(-x,-y,z));  // 5
        positionArray.push_back(vec3(-x,-y,-z)); // 6
        positionArray.push_back(vec3(x,-y,-z));  // 7
        size+=4;

        // front
        positionArray.push_back(vec3(x,y,z));    // 8
        positionArray.push_back(vec3(-x,y,z));   // 9
        positionArray.push_back(vec3(-x,-y,z));  // 10
        positionArray.push_back(vec3(x,-y,z));   // 11
        size+=4;

        // back
        positionArray.push_back(vec3(-x,-y,-z)); // 12
        positionArray.push_back(vec3(-x,y,-z));  // 13
        positionArray.push_back(vec3(x,y,-z));   // 14
        positionArray.push_back(vec3(x,-y,-z));  // 15
        size+=4;

        // right
        positionArray.push_back(vec3(x,-y,z));   // 16
        positionArray.push_back(vec3(x,-y,-z));  // 17
        positionArray.push_back(vec3(x,y,-z));   // 18
        positionArray.push_back(vec3(x,y,z));     // 19
        size+=4;

        // left
        positionArray.push_back(vec3(-x,-y,z));  // 20
        positionArray.push_back(vec3(-x,y,z));   // 21
        positionArray.push_back(vec3(-x,y,-z));  // 22
        positionArray.push_back(vec3(-x,-y,-z)); // 23
        size+=4;
    }

    GLuint restart = 0xFFFFFFFF;
    for(GLuint i = 0; i < size; i++) {
        indexArray.push_back(i);
        indices++;
        if (i%4==3) {
            indexArray.push_back(restart);
            indices++;
        }
    }
    indexArray.push_back(restart);
    for(int i = 0; i < size/4; i++) {
        int plane = 4*i;
        vec3 a = positionArray.at(plane) - positionArray.at(plane+1);
        vec3 b = positionArray.at(plane) - positionArray.at(plane+2);
        vec3 n = cross(a,b);
        normalArray.push_back(n);
        normalArray.push_back(n);
        normalArray.push_back(n);
        normalArray.push_back(n);
    }
}

void Cube::initColors() {
    GLint brickColorLoc = widget->glGetUniformLocation(program, "brickColor");
    GLint brickSizeLoc = widget->glGetUniformLocation(program, "brickSize");
    GLint percentBrickLoc = widget->glGetUniformLocation(program, "percentBrick");
    GLint scalarLoc = widget->glGetUniformLocation(program,"scalar");

    widget->glUniform3f(brickSizeLoc,0.3,0.15,0.2);
    widget->glUniform3f(percentBrickLoc,0.9,0.9,0.9);
    widget->glUniform3f(scalarLoc,1.0,1.0,1.0);
    widget->glUniform3f(brickColorLoc,0.9,0,0);

    widget->glUniform3f(lightPosLoc, 1,-20,1000);
    widget->glUniform3f(lightPos2Loc, 1,-10,5000);
    widget->glUniform3f(lightColorLoc, 1,1,1);
    widget->glUniform1f(lightIntensityLoc, 1);

    widget->glUniform3f(ambientColorLoc, 0.1, 0.1, 0.1);
    widget->glUniform3f(diffuseColorLoc, 1.2, 1.2, 1.2);
    widget->glUniform3f(specularColorLoc, 0.6,0.2,0.2);
}
