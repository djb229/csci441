#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>
#include <QTimer>

class MyShapes;
class Cube;
class Torus;
class Wall;
class RabbitShip;
class Asteroids;
class Sun;

#define GLM_FORCE_RADIANS

using glm::mat4;

class GLWidget : public QOpenGLWidget, public QOpenGLFunctions_3_3_Core {
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);

    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);
        void keyReleaseEvent(QKeyEvent *event);

    protected slots:
        void animate();

    private:
        void initializeWall();
        void loadBMP(const char* bmp, GLuint texture);

        void initializeSquareWallVao(std::vector<unsigned int> &indices,
                                         std::vector<glm::vec3> &positions,
                                         std::vector<glm::vec3> &normals);
        void initializeWallVao(std::vector<unsigned int> &indices,
                               std::vector<glm::vec3> &positions,
                               std::vector<glm::vec3> &normals);
        void initializeStars();
        void renderWall();

        int shape;
        int modelSize;
        int squareModelSize;

        glm::vec3 brickSize;
        glm::vec3 percentBrick;
        glm::vec3 scalar;

        GLuint wallProg;
        GLuint wallVao;
        GLuint squareVao;

        GLuint starsProg;
        GLuint starsVao;
        GLint starColorLoc;
        GLint starColorArray[600];
        int starColorIndex;

        GLuint textures[3];

        GLint modelProjMatrixLoc;
        GLint modelViewMatrixLoc;
        GLint modelModelMatrixLoc;

        GLint modelLightPosLoc;
        GLint modelLightColorLoc;
        GLint modelLightIntensityLoc;

        GLint modelDiffuseColorLoc;
        GLint modelAmbientColorLoc;

        GLint brickColorLoc;
        GLint brickSizeLoc;
        GLint percentBrickLoc;
        GLint scalarLoc;

        void initializeGrass();
        void renderGrass();
        void renderSky();

        GLint starsProjMatrixLoc;
        GLint starsViewMatrixLoc;
        GLint starsModelMatrixLoc;

        GLuint grassProg;
        GLuint grassVao;
        GLuint coneProg;
        GLuint coneVao;
        GLuint topProg;
        GLuint topVao;

        GLint grassProjMatrixLoc;
        GLint grassViewMatrixLoc;
        GLint grassModelMatrixLoc;

        GLint grassLightPosLoc;
        GLint grassLightPos2Loc;
        GLint grassLightColorLoc;
        GLint grassLightIntensityLoc;
        GLint grassDiffuseColorLoc;
        GLint grassAmbientColorLoc;

        GLint topProjMatrixLoc;
        GLint topViewMatrixLoc;
        GLint topModelMatrixLoc;

        GLint topLightPosLoc;
        GLint topLightPos2Loc;
        GLint topLightColorLoc;
        GLint topLightIntensityLoc;
        GLint topDiffuseColorLoc;
        GLint topAmbientColorLoc;

        GLint coneProjMatrixLoc;
        GLint coneViewMatrixLoc;
        GLint coneModelMatrixLoc;

        GLint coneLightPosLoc;
        GLint coneLightPos2Loc;
        GLint coneLightColorLoc;
        GLint coneLightIntensityLoc;
        GLint coneDiffuseColorLoc;
        GLint coneAmbientColorLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;

        mat4 pitchMatrix;
        mat4 yawMatrix;
        mat4 translateMatrix;

        float pitchAngle;
        float yawAngle;

        QTimer *timer;

        bool left;
        bool right;
        bool forward;
        bool backward;
        bool up;
        bool down;
        bool fly;
        bool turnLeft;
        bool turnRight;
        bool moveUp;
        bool moveDown;

        glm::vec3 viewPosition;
        glm::vec3 velocity;
        glm::vec3 vectorRight;
        glm::vec3 vectorForward;

        int width;
        int height;

        int grassSize;
        int grassIndexSize;

        int topSize;
        int topIndexSize;

        int coneSize;
        int coneIndexSize;

        glm::vec3 lastVPt;
        glm::vec3 lastLightPt;
        glm::vec2 lastPt;
        glm::vec3 pointOnVirtualTrackball(const glm::vec2 &pt);


        // Pointers for MyShapes objects
        Cube *cube;
        Wall *wall;
        Torus *torus;
        Torus *torus2;
        Torus *torus3;
        RabbitShip *rabbit;
        Asteroids *asteroids;
        Sun *sun;
};

#endif
