#ifndef CUBE_H
#define CUBE_H

#include "myshapes.h"

class Cube : public MyShapes {

public:
    struct asteroid;

    Cube(GLWidget *myWidget, glm::vec3 scale, glm::vec3 shift);
    ~Cube();

    void constructShape(vector<glm::vec3> &positionArray,
                        vector<glm::vec3> &normalArray,
                        vector<GLuint> &indexArray);
    void initColors();

    bool sides[6];
};

#endif // CUBE_H
