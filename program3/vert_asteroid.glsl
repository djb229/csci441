#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 scalar;

in vec3 shift;
in vec3 position;
in vec3 normal;

out vec3 pos;
out vec3 norm;
out vec3 MCposition;

void main() {
  vec3 shamt = shift;
  MCposition = scalar * position;
  gl_Position = projection * view * model * vec4((position+shamt) * scalar, 1);
  norm = (transpose(inverse(model))*vec4(normal,0)).xyz;
  pos = (model*vec4((position+shamt)*scalar,1)).xyz;
}
