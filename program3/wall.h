#ifndef WALL_H
#define WALL_H

#include "myshapes.h"

class Wall : public MyShapes {

public:
    Wall(GLWidget *myWidget, glm::vec3 scale, glm::vec3 shift);
    ~Wall();

    void constructShape(vector<glm::vec3> &positionArray, vector<glm::vec3> &normalArray, vector<GLuint> &indexArray);
    void initColors();
};
#endif // WALL_H
