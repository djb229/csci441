#ifndef ASTEROIDS_H
#define ASTEROIDS_H

#include "myshapes.h"

class Asteroids : public MyShapes {
public:

    struct asteroid{
        vec3 origin;    // Where it starts
        vec3 position;  // Where it is
        vec3 velocity;  // Where it's going

        vector<vec3> vertices;  // Shape
        vector<vec3> norms;     // Vertex normals
        vector<GLuint> inds;    // Indices
    };

    Asteroids(GLWidget *myWidget, glm::vec3 scale, glm::vec3 shift);
    ~Asteroids();

    void constructShape(vector<vec3> &positionArray,
                        vector<vec3> &normalArray,
                        vector<GLuint> &indexArray);
    void initColors();
    void render();

    void emitParticles(vector<vec3> &shift);

    vector<asteroid> particles;

    int renderCount;

    GLuint shiftBuffer;
};

#endif // ASTEROIDS_H
