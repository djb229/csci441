/*
 * Interface for loading shapes using GLSL shaders and OpenGL.
 * By David Bell
 *
 * Parent class for shape objects.
 *
 * Constructor requires a GLWidget and two constant char array pointers as arguments.
 * The char arrays must be the file paths of the vertex and fragment shaders that will
 * render this shape.
 *
 * Handles most of the loading of values onto the GPU. Does not handle textures.
 *
 * Children will reimplement:
 *          initColors() to specify the object's color variables;
 *          constructShape() to build the vertex, normal, and index arrays;
 *
 * Children might reimplement:
 *          render() to specify how the shape needs to be rendered (default is GL_TRIANGLE_FAN);
 *
 * Global variables should be fairly intuitive:
 *   GLWidget widget;           -- GLWidget that draws this shape
 *   GLuint program;            -- Shader program for this shape on the GPU
 *   GLuint vao;                -- OpenGL Vertex Array Object to store data
 *   GLint modelLoc;            -- Model matrix uniform location: vert/frag shader's "model"
 *   GLint viewLoc;             -- View matrix uniform location: vert/frag shader's "view"
 *   GLint projectionLoc;       -- Projection matrix uniform location: vert/frag shader's "projection"
 *
 *   GLint lightPosLoc;         -- Light position uniform location: frag shader's "lightPos"
 *   GLint lightPos2Loc;        -- Second light position uniform location: frag shader's "lightPos2"
 *   GLint lightColorLoc;       -- Light color uniform location: frag shader's "lightColor"
 *   GLint lightIntensityLoc;   -- Light intensity uniform location: frag shader's "lightIntensity"
 *   GLint diffuseColorLoc;     -- Diffuse color uniform location: frag shader's "diffuseColor"
 *   GLint ambientColorLoc;     -- Ambient color uniform location: frag shader's "ambientColor"
 *   GLint specularColorLoc;    -- Specular color uniform location: frag shader's "specularColor"
 *
 *   int size;                  -- Number of vertices in this shape
 *   int indices;               -- Number of indices to draw this shape
 */

#include "myshapes.h"

using glm::vec3;

MyShapes::MyShapes(GLWidget *myWidget, const char *vertShader, const char *fragShader) {

    widget = myWidget;

    // Load our vertex and fragment shaders into a program object on the GPU
    program = widget->loadShaders(vertShader, fragShader);
    widget->glUseProgram(program);

    // Load cone position, normal, UV position, and index data onto the GPU
    widget->glGenVertexArrays(1, &vao); // Create vertex array object to hold cone vertex data
    widget->glBindVertexArray(vao);

    // Get model, view, and projection matrix locations
    projectionLoc = widget->glGetUniformLocation(program, "projection");
    viewLoc = widget->glGetUniformLocation(program, "view");
    modelLoc = widget->glGetUniformLocation(program, "model");

    // Get lighting uniform locations
    lightPosLoc = widget->glGetUniformLocation(program, "lightPos");
    lightPos2Loc = widget->glGetUniformLocation(program, "lightPos2");
    lightColorLoc = widget->glGetUniformLocation(program, "lightColor");
    lightIntensityLoc = widget->glGetUniformLocation(program, "lightIntensity");

    ambientColorLoc = widget->glGetUniformLocation(program, "ambientColor");
    diffuseColorLoc = widget->glGetUniformLocation(program, "diffuseColor");
    specularColorLoc = widget->glGetUniformLocation(program, "specularColor");
}

MyShapes::~MyShapes() {}

/*
 * Render the shape
 */
void MyShapes::render() {
    widget->glUseProgram(program);
    widget->glBindVertexArray(vao);
    if (indices != 0) {
        widget->glDrawElements(drawMethod, indices, GL_UNSIGNED_INT, 0);
    } else if (size != 0) {
        widget->glDrawArrays(drawMethod,0,size);
    }
}

/*
 * Set lighting and color values. Defaults to red.
 */
void MyShapes::initColors() {
    widget->glUniform3f(lightPosLoc, 1,-20,100);
    widget->glUniform3f(lightPos2Loc, 1,-10,50);
    widget->glUniform3f(lightColorLoc, 1.0,0,0);
    widget->glUniform1f(lightIntensityLoc, 1.0);
    widget->glUniform3f(ambientColorLoc, 1.0, 0, 0);
    widget->glUniform3f(diffuseColorLoc, 1.0, 0, 0);
    widget->glUniform3f(specularColorLoc, 1.0,0,0);
}

/*
 * Define the shape's vertices, normals, and indices.
 *
 * Sets input as pointers to null arrays by default.
 *
 * Reimplementations should assign each pointer chain
 * a pointer to an array and change the value of the
 * global variables size and indices.
 *
 * Example:
 *
 *   // Set vertex positions
 *   vec3 position[] = { vec3(),vec3(),...,vec3() };
 *
 *   size = position.length;
 *
 *   // Calculate vertex normals
 *   vec3 normal[size];
 *   for (int i = 0; ...
 *   ...
 *   ...
 *
 *   // Set vertex indices
 *   GLuint index[] = { 0,1,2,3,restart,0,1,...,restart };
 *
 *   indices = index.length;
 *
 *   // Set input pointers to the addresses of the new arrays
 *   positionArray = &position;
 *   normalArray = &normal;
 *   indexArray = &index;
 */
void MyShapes::constructShape(vector<vec3> &positionArray,vector<vec3> &normalArray,vector<GLuint> &indexArray) {
    printf("Must implement constructShape method/n");
}

/*
 * Upload vertex positions to GPU.
 */
void MyShapes::initPositions(vector<vec3> &positionArray) {
    if(size != 0) {
        // Create a buffer on the GPU for grass vertex coordinates
        widget->glGenBuffers(1, &positionBuffer);
        widget->glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
        widget->glBufferData(GL_ARRAY_BUFFER, positionArray.size()*sizeof(vec3), &positionArray[0], GL_STATIC_DRAW);
        // Bind attribute "position" to grassPosBuffer
        positionLoc = widget->glGetAttribLocation(program, "position");
        widget->glEnableVertexAttribArray(positionLoc);
        widget->glVertexAttribPointer(positionLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }
}

/*
 * Upload vertex indices to GPU.
 */
void MyShapes::initIndices(vector<GLuint> &indexArray) {
    if(indices != 0) {
        // Create a buffer on the GPU for grass vertex indices
        widget->glGenBuffers(1, &indexBuffer);
        widget->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        widget->glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexArray.size()*sizeof(GLuint), &indexArray[0], GL_STATIC_DRAW);
    }
}

/*
 * Upload vertex normals to GPU.
 */
void MyShapes::initNormals(vector<vec3> &normalArray) {
    if(size != 0) {
        // Create a buffer on the GPU for vertex normals
        widget->glGenBuffers(1, &normBuffer);
        widget->glBindBuffer(GL_ARRAY_BUFFER, normBuffer);
        widget->glBufferData(GL_ARRAY_BUFFER, normalArray.size()*sizeof(vec3), &normalArray[0], GL_STATIC_DRAW);
        // Bind attribute "normal" to normalBuffer
        normLoc = widget->glGetAttribLocation(program, "normal");
        widget->glEnableVertexAttribArray(normLoc);
        widget->glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }
}

void MyShapes::initialize() {

    widget->glUseProgram(program);
    widget->glBindVertexArray(vao);

    vector<vec3> positionArray;
    vector<vec3> normalArray;
    vector<GLuint> indexArray;

    constructShape(positionArray,normalArray,indexArray);

    size = positionArray.size();
    indices = indexArray.size();

    initColors();

    initPositions(positionArray);
    initNormals(normalArray);
    initIndices(indexArray);
}

/*
 * Overloaded method to update model, view, and projection matrices.
 *
 * Call using glm::value_ptr(glm::mat4 newMatrix) to update a matrix
 * and boolean values (false for readability) to not update a matrix.
 */
void MyShapes::updateMatrices(const GLfloat *m,bool v,bool p) {
    widget->glUseProgram(program);
    widget->glUniformMatrix4fv(modelLoc, 1, false, m);
}
void MyShapes::updateMatrices(bool m,const GLfloat *v,bool p) {
    widget->glUseProgram(program);
    widget->glUniformMatrix4fv(viewLoc, 1, false, v);
}
void MyShapes::updateMatrices(bool m,bool v,const GLfloat *p){
    widget->glUseProgram(program);
    widget->glUniformMatrix4fv(projectionLoc, 1, false, p);
}
void MyShapes::updateMatrices(const GLfloat *m, const GLfloat *v, bool p){
    widget->glUseProgram(program);
    widget->glUniformMatrix4fv(modelLoc, 1, false, m);
    widget->glUniformMatrix4fv(viewLoc, 1, false, v);
}
void MyShapes::updateMatrices(const GLfloat *m,bool v,const GLfloat *p){
    widget->glUseProgram(program);
    widget->glUniformMatrix4fv(modelLoc, 1, false, m);
    widget->glUniformMatrix4fv(projectionLoc, 1, false, p);
}
void MyShapes::updateMatrices(bool m, const GLfloat *v, const GLfloat *p){
    widget->glUseProgram(program);
    widget->glUniformMatrix4fv(viewLoc, 1, false, v);
    widget->glUniformMatrix4fv(projectionLoc, 1, false, p);
}
void MyShapes::updateMatrices(const GLfloat *m,const GLfloat *v,const GLfloat *p){
    widget->glUseProgram(program);
    widget->glUniformMatrix4fv(modelLoc, 1, false, m);
    widget->glUniformMatrix4fv(viewLoc, 1, false, v);
    widget->glUniformMatrix4fv(projectionLoc, 1, false, p);
}

bool loadOBJ(const char *path,std::vector < glm::vec3 > &positionArray,
             std::vector < glm::vec3 > &uvArray,std::vector < glm::vec3 > &normalArray) {
    std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
    std::vector< glm::vec3 > tempVertices;
    std::vector< glm::vec2 > tempUV;
    std::vector< glm::vec3 > tempNormals;

    FILE * file = fopen(path, "r");
    if( file == NULL ){
        printf("Impossible to open the file !\n");
        return false;
    }


    while( true ){

        char lineHeader[128];
        // read the first word of the line
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF) {
            break; // EOF = End Of File. Quit the loop.
        }

        if ( strcmp( lineHeader, "v" ) == 0 ){
            vec3 vertex;
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z );
            tempVertices.push_back(vertex);
        }else if ( strcmp( lineHeader, "vt" ) == 0 ){
            glm::vec2 uv;
            fscanf(file, "%f %f\n", &uv.x, &uv.y );
            tempUV.push_back(uv);
        }else if ( strcmp( lineHeader, "vn" ) == 0 ){
            vec3 normal;
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z );
            tempNormals.push_back(normal);
        }else if ( strcmp( lineHeader, "f" ) == 0 ){
            string vertex1, vertex2, vertex3;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2] );
            if (matches != 9){
                printf("File can't be read by our simple parser : ( Try exporting with other options\n");
                return false;
            }
            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);
            uvIndices.push_back(uvIndex[0]);
            uvIndices.push_back(uvIndex[1]);
            uvIndices.push_back(uvIndex[2]);
            normalIndices.push_back(normalIndex[0]);
            normalIndices.push_back(normalIndex[1]);
            normalIndices.push_back(normalIndex[2]);
        }
    }
    // For each vertex of each triangle
    for( unsigned int i=0; i<vertexIndices.size(); i++ ){

        unsigned int vertexIndex = vertexIndices[i]-1;
        glm::vec3 vertex = tempVertices[vertexIndex];
        positionArray.push_back(vertex);

        unsigned int normalIndex = normalIndices[i]-1;
        glm::vec3 normal = tempNormals[normalIndex];
        normalArray.push_back(normal);

        unsigned int uvIndex = uvIndices[i]-1;
        glm::vec3 uv = vec3(tempUV[uvIndex].x,tempUV[uvIndex].y,0);
        uvArray.push_back(uv);
    }
}


void MyShapes::updateLightPosition(vec3 loc, bool lightPos2) {
    widget->glUseProgram(program);
    if(lightPos2) {
        widget->glUniform3f(lightPos2Loc, loc.x,loc.y,loc.z);
    } else {
        widget->glUniform3f(lightPosLoc, loc.x,loc.y,loc.z);
    }
}
