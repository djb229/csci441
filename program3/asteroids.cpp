#include "asteroids.h"
#include <iostream>
#include <QTextStream>
#include <tinyobjloader/tiny_obj_loader.h>
#include <functional>
#include <random>

using glm::vec3;
using glm::cross;

Asteroids::Asteroids(GLWidget *myWidget, glm::vec3 scale, glm::vec3 shift) :
    MyShapes(myWidget, ":/vert_asteroid.glsl", ":/frag_asteroid.glsl") {

    drawMethod = GL_TRIANGLE_FAN;

    initialize();
}

Asteroids::~Asteroids()
{

}

void Asteroids::render() {

    widget->glUseProgram(program);
    widget->glBindVertexArray(vao);

    vector<vec3> shift;
    emitParticles(shift);

    widget->glBindBuffer(GL_ARRAY_BUFFER, shiftBuffer);
    widget->glBufferData(GL_ARRAY_BUFFER, shift.size()*sizeof(vec3), &shift[0], GL_STATIC_DRAW);

    // Bind attribute "shift" to shiftBuffer
    GLuint shiftLoc = widget->glGetAttribLocation(program, "shift");
    widget->glEnableVertexAttribArray(shiftLoc);
    widget->glVertexAttribPointer(shiftLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);

    widget->glDrawElements(drawMethod, indices, GL_UNSIGNED_INT, 0);
}

void Asteroids::initColors() {

    GLint brickColorLoc = widget->glGetUniformLocation(program, "brickColor");
    GLint brickSizeLoc = widget->glGetUniformLocation(program, "brickSize");
    GLint percentBrickLoc = widget->glGetUniformLocation(program, "percentBrick");
    GLint scalarLoc = widget->glGetUniformLocation(program,"scalar");

    widget->glUniform3f(brickSizeLoc,0.3,0.15,0.2);
    widget->glUniform3f(percentBrickLoc,0.9,0.9,0.9);
    widget->glUniform3f(scalarLoc,1.0,1.0,1.0);
    widget->glUniform3f(brickColorLoc,0.9,0,0);

    widget->glUniform3f(lightPosLoc, 1,-20,1000);
    widget->glUniform3f(lightPos2Loc, 1,-10,5000);
    widget->glUniform3f(lightColorLoc, 1,1,1);
    widget->glUniform1f(lightIntensityLoc, 1);

    widget->glUniform3f(ambientColorLoc, 0.1, 0.1, 0.1);
    widget->glUniform3f(diffuseColorLoc, 1.2, 1.2, 1.2);
    widget->glUniform3f(specularColorLoc, 0.6,0.2,0.2);
}

void Asteroids::constructShape(vector<vec3> &positionArray,
                          vector<vec3> &normalArray,
                          vector<GLuint> &indexArray) {

    std::default_random_engine engine;
    std::uniform_real_distribution<float> distribution(0.5,2.0);
    auto random = std::bind(distribution,engine);

    float genPositions[20];
    for(int i = 0; i < 20; i++) {
        genPositions[i] = random()*44;
    }

    vector<vec3>shamt;

    std::default_random_engine engine2;
    std::uniform_int_distribution<int> distribution2(0,20);
    auto randSpawn = std::bind(distribution2,engine2);

    int lastPosX = 0;
    int lastPosY = 0;
    for(int i = 0; i < 200; i++) {

        int indX = randSpawn();
        int indY = randSpawn();

        if(indX == lastPosX && indY == lastPosY)  {
            indX = randSpawn();
            indY = randSpawn();
        }

        lastPosX = indX;
        lastPosY = indY;

        float posX = genPositions[indX];
        float posY = genPositions[indY];

        float negateX = random()-1.25;
        float negateY = random()-1.25;

        if(negateX<0) {
            posX *= -1.0;
        }
        if(negateY<0) {
            posY *= -1.0;
        }

        Asteroids::asteroid a;

        a.origin = vec3(posX,posY,-100);
        a.position = vec3(a.origin.x,a.origin.y,-100.0+float(i));
        a.velocity = vec3(0,0,0.25);

        vec3 brt = vec3(random(),random(),random());
        vec3 brb = vec3(random(),-random(),random());
        vec3 frt = vec3(random(),random(),-random());
        vec3 frb = vec3(random(),-random(),-random());
        vec3 blt = vec3(-random(),random(),random());
        vec3 blb = vec3(-random(),-random(),random());
        vec3 flt = vec3(-random(),random(),-random());
        vec3 flb = vec3(-random(),-random(),-random());

        // top
        a.vertices.push_back(brt);    // 0
        a.vertices.push_back(frt);   // 1
        a.vertices.push_back(flt);  // 2
        a.vertices.push_back(blt);   // 3

        // bottom
        a.vertices.push_back(brb);   // 4
        a.vertices.push_back(blb); // 5
        a.vertices.push_back(flb); // 6
        a.vertices.push_back(frb);  // 7

        // front
        a.vertices.push_back(brt);    // 8
        a.vertices.push_back(blt);   // 9
        a.vertices.push_back(blb);  // 10
        a.vertices.push_back(brb);   // 11

        // back
        a.vertices.push_back(flb); // 12
        a.vertices.push_back(flt);  // 13
        a.vertices.push_back(frt);   // 14
        a.vertices.push_back(frb);  // 15

        // right
        a.vertices.push_back(brb);   // 16
        a.vertices.push_back(frb);  // 17
        a.vertices.push_back(frt);   // 18
        a.vertices.push_back(brt);     // 19

        // left
        a.vertices.push_back(blb);  // 20
        a.vertices.push_back(blt);   // 21
        a.vertices.push_back(flt);  // 22
        a.vertices.push_back(flb); // 23

        GLuint restart = 0xFFFFFFFF;

        a.inds.push_back(i*24);
        indexArray.push_back(i*24);
        positionArray.push_back(a.vertices.at(0));
        shamt.push_back(a.position);

        for(unsigned int j = 1; j < 24; j++) {
            positionArray.push_back(a.vertices.at(j));
            shamt.push_back(a.position);
            if(j%4==0) {
                a.inds.push_back(restart);
                indexArray.push_back(restart);
            }
            a.inds.push_back(j);
            indexArray.push_back(i*24+j);
        }
        a.inds.push_back(restart);
        indexArray.push_back(restart);


        for(int i = 0; i < 6; i++) {
            vec3 n;
            vec3 c = a.vertices[4*i+0] - a.vertices[4*i+1];
            vec3 b = a.vertices[4*i+0] - a.vertices[4*i+2];
            n = cross(c,b);

            a.norms.push_back(n);
            a.norms.push_back(n);
            a.norms.push_back(n);
            a.norms.push_back(n);
            normalArray.push_back(n);
            normalArray.push_back(n);
            normalArray.push_back(n);
            normalArray.push_back(n);
        }
        particles.push_back(a);
    }

    // Create a buffer on the GPU for position shift array
    widget->glGenBuffers(1, &shiftBuffer);
    widget->glBindBuffer(GL_ARRAY_BUFFER, shiftBuffer);
    widget->glBufferData(GL_ARRAY_BUFFER, shamt.size()*sizeof(vec3), &shamt[0], GL_STATIC_DRAW);

    GLuint shiftLoc = widget->glGetAttribLocation(program, "shift");
    widget->glEnableVertexAttribArray(shiftLoc);
    widget->glVertexAttribPointer(shiftLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);



}

void Asteroids::emitParticles(vector<vec3> &shift) {
    shift.clear();
    for(int i = 0; i < particles.size(); i++) {
        asteroid a = particles[i];
        if(a.position.z > 100) {
            a.position = vec3(a.position.x,a.position.y,a.position.z-200);
        } else {
            a.position = vec3(a.position.x,a.position.y,a.position.z+a.velocity.z);
        }

        particles.at(i) = a;
        for(int j = 0; j < 24;j++) {
            shift.push_back(a.position);
        }
    }
}
