#ifndef SUN_H
#define SUN_H

#include "myshapes.h"

class Sun : public MyShapes
{
public:
    Sun(GLWidget *myWidget);
    ~Sun();
    void constructShape(vector<glm::vec3> &positionArray,
                        vector<glm::vec3> &normalArray,
                        vector<GLuint> &indexArray);
    void initColors();

};

#endif // SUN_H
