#version 330
uniform int colorChange;

in vec3 pos;

out vec4 color_out;

void main() {

    int x = int(pos.x);
    int y = int(pos.y);
    int z = int(pos.z);
    float xPrecision = float(x)/pos.x;
    float yPrecision = float(y)/pos.y;
    float zPrecision = float(z)/pos.z;

    int chooseColor = (x+y+z+colorChange)%3;
    if(chooseColor == 0) {
        color_out = vec4(xPrecision,0.5*yPrecision,1.0,1.0);
    } else if (chooseColor == 1) {
        color_out = vec4(0.5*xPrecision,yPrecision,1.0,1.0);
    } else {
        float sum = (xPrecision+yPrecision+zPrecision)/3.0;
        color_out = vec4(sum,sum, 1.0, 1.0);
    }
}
