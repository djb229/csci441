#ifndef MYSHAPES_H
#define MYSHAPES_H

#include "glwidget.h"

using glm::vec3;

using namespace std;

class MyShapes {
public:
    MyShapes(GLWidget *myWidget, const char *vertShader, const char *fragShader);
    ~MyShapes();

    virtual void render();

    void updateLightPosition(vec3 loc, bool lightPos2);

    void updateMatrices(const GLfloat *m, bool v, bool p);
    void updateMatrices(bool m,const GLfloat *v,bool p);
    void updateMatrices(bool m, bool v, const GLfloat *p);
    void updateMatrices(const GLfloat *m,const GLfloat *v,bool p);
    void updateMatrices(const GLfloat *m, bool v, const GLfloat *p);
    void updateMatrices(bool m,const GLfloat *v,const GLfloat *p);
    void updateMatrices(const GLfloat *m, const GLfloat *v, const GLfloat *p);

    virtual void constructShape(vector<vec3> &positionArray,vector<vec3> &normalArray,vector<GLuint> &indexArray);
    virtual void initColors();

    void initialize();

    void initPositions(vector<vec3> &positionArray);
    void initIndices(vector<GLuint> &indexArray);
    void initNormals(vector<vec3> &normalArray);

    bool loadOBJ(const char * path,std::vector < glm::vec3 > & out_vertices,
        std::vector < glm::vec2 > & out_uvs,std::vector < glm::vec3 > & out_normals);

    GLWidget *widget;

    GLuint program;
    GLuint vao;

    GLint modelLoc;
    GLint viewLoc;
    GLint projectionLoc;

    GLint lightPosLoc;
    GLint lightPos2Loc;
    GLint lightColorLoc;
    GLint lightIntensityLoc;

    GLint diffuseColorLoc;
    GLint ambientColorLoc;
    GLint specularColorLoc;

    GLuint positionBuffer;
    GLuint normBuffer;
    GLuint indexBuffer;
    GLint positionLoc;
    GLint normLoc;

    int size;
    int indices;
    vec3 translate;
    vec3 scale;

    GLenum drawMethod;
};

#endif // MYSHAPES_H
