#include <QtGui/QImage>
#include <QtGui/QColor>
#include <iostream>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int main(int argc, char** argv)
{
    string input [3] = {};
    int x [3] = {};
    int y [3] = {};
    float r [3] = {};
    float g [3] = {};
    float b [3] = {};
    cout << "Enter three points with rgb values (in the form x,y:r,g,b):\n";
    getline (cin,input[0]);
    getline (cin,input[1]);
    getline (cin,input[2]);

    cout << "Entered: \n";
    for (int i = 0; i < 3; i++) {
        char comma1, colon, comma2, comma3;
        istringstream ss(input[i]);
        ss >> x[i] >> comma1 >> y[i] >> colon >> r[i] >> comma2 >> g[i] >> comma3 >> b[i];
        cout << x[i] << comma1 << y[i] << colon << r[i] << comma2 << g[i] << comma3 << b[i] << "\n"; 
    }

    QImage image(640,480,QImage::Format_RGB32);
    
    float minX = (float) min(x[0],x[1]);
    minX = min(minX, (float) x[2]);
    float maxX = (float) max(x[0],x[1]);
    maxX = max(maxX, (float) x[2]);

    float minY = (float) min(y[0],y[1]);
    minY = min(minY, (float) y[2]);
    float maxY = (float) max(y[0],y[1]);
    maxY = max(maxY, (float) y[2]);

    for (int i = (int) minY; i < (int) maxY; i++) {

        for (int j = (int) minX; j < (int) maxX; j++) {
            float lam1 = (float)((y[1]-y[2])*(j-x[2])+(x[2]-x[1])*(i-y[2]))/(float)((y[1]-y[2])*(x[0]-x[2])+(x[2]-x[1])*(y[0]-y[2]));
            float lam2 = (float)((y[2]-y[0])*(j-x[2])+(x[0]-x[2])*(i-y[2]))/(float)((y[1]-y[2])*(x[0]-x[2])+(x[2]-x[1])*(y[0]-y[2]));
            float lam3 = 1.0f - lam1 - lam2;
            if (lam1 > 0.0f && lam2 > 0.0f && lam3 > 0.0f && lam1 < 1.0f && lam2 < 1.0f && lam3 < 1.0f) {
                float red = (r[0]*lam1 + r[1]*lam2 + r[2]*lam3)/(float) max(1.0f, (r[0]+r[1]+r[2]));
                float green = (g[0]*lam1 + g[1]*lam2 + g[2]*lam3)/(float) max(1.0f, (g[0]+g[1]+g[2]));
                float blue = (b[0]*lam1 + b[1]*lam2 + b[2]*lam3)/(float) max(1.0f, (b[0]+b[1]+b[2]));
                image.setPixel(j,i,qRgb(255*red,255*green,255*blue));
            }
        }
    }

    if (image.save("triangle.jpg",0,100)) {
        cout << "Output triangle.jpg" << endl;
    } else {
        cout << "Unable to save triangle.jpg" << endl;
    }

    return 0;
}
