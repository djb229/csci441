#include "glwidget.h"
#include <iostream>
#include <sstream>

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    string input [3] = {};
    float x [3] = {};
    float y [3] = {};
    float r [3] = {};
    float g [3] = {};
    float b [3] = {};
    cout << "Enter three points with rgb values (in the form x,y:r,g,b):\n";
    getline (cin,input[0]);
    getline (cin,input[1]);
    getline (cin,input[2]);

    cout << "Entered: \n";
    Point p[3];
    for (int i = 0; i < 3; i++) {
        char comma1, colon, comma2, comma3;
        istringstream ss(input[i]);
        ss >> x[i] >> comma1 >> y[i] >> colon >> r[i] >> comma2 >> g[i] >> comma3 >> b[i];
        cout << x[i] << comma1 << y[i] << colon << r[i] << comma2 << g[i] << comma3 << b[i] << "\n";

        p[i] = Point(x[i], y[i]);
        p[i] = w2nd(p[i]);
    }

    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // position data for a single triangle
    Point pts[3] = {
        p[0],
        p[1],
        p[2]
    };

    GLuint color_buffer;
    GLfloat colors[] = {
       r[0], g[0], b[0], 1.0,
       r[1], g[1], b[1], 1.0,
       r[2], g[2], b[2], 1.0
     };
     glGenBuffers(1, &color_buffer);
     glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
     glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);


    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    // Upload the position data to the GPU, storing
    // it in the buffer we just created.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders();

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
    GLint colorIndex =  glGetAttribLocation(program, "color_in");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 4, GL_FLOAT, GL_FALSE, 0, 0);
}

void GLWidget::resizeGL(int w, int h) {
    glViewport(0,0,w,h);
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

GLuint GLWidget::loadShaders() {
    GLuint program = glCreateProgram();

    const GLchar* vertSource = 
        "#version 330\n"
        "in vec2 position;\n"
        "in vec4 color_in;\n"
        "out vec4 color;\n"
        "void main() {\n"
        "  gl_Position = vec4(position.x, position.y, 0, 1);\n"
        "  color = color_in;\n"
        "}\n";

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);
    
    const GLchar* fragSource = 
        "#version 330\n"
        "in vec4 color;\n"
        "out vec4 color_out;\n"
        "void main() {\n"
        "  color_out = color;\n"
        "}\n";
    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    glUseProgram(program);

    return program;
}

Point GLWidget::w2nd(Point pt_w) {
    /* convert pt_w to normalized device coordinates */
    /* use this method to convert your input coordinates to
       normalized device coordinates */
    pt_w.x = -1.0f+pt_w.x*(2.0f/640.0f);
    pt_w.y = 1.0f-pt_w.y*(2.0f/480.0f);
    return pt_w;
}


